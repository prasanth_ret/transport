package com.ret.transport.base.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.ret.transport.base.enums.EDateTimeType;
import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class DateConversionUtil {

    public static String convertLocalDateTimeToDate(LocalDateTime localDateTime, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return localDateTime.format(formatter);
    }

    public static String convertDate(Date date, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        if (date != null) {
            return dateFormat.format(date);
        }
        return null;
    }

    public static Date convertDate(Date date, EDateTimeType eTimeType) {
        Date result = null;
        if (Validation.isNotNull(date)) {
            LocalDateTime localDateTime = convertDateToLocalDateTime(date, eTimeType);
            result = convertLocalDateTimeToDate(localDateTime);
        }
        return result;
    }

    public static LocalDateTime convertDateToLocalDateTime(Date date) {
        if (date != null) {
            return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        }
        return null;
    }

    public static Date convertLocalDateTimeToDate(LocalDateTime localDateTime) {
        if (localDateTime != null) {
            Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
            return Date.from(instant);
        }
        return null;
    }

    public static String convertLocalDateTimeToTime(LocalDateTime localDateTime, String format) {
        if (localDateTime != null) {
            return String.valueOf(localDateTime.format(DateTimeFormatter.ofPattern(format)));
        }
        return null;
    }

    /*
     * <<<<<<< HEAD Get LocalDateTime for the given date with Specific time (End of day-23:59:59,
     * Start of day-00:00:00) ======= Get LocalDateTime for the given date with Specific time (End
     * of day-23:59:59, Start of day-00:00:00) >>>>>>> c819e8a62dddcec8799c6831542d8e931de9c5ee
     */
    public static LocalDateTime convertDateToLocalDateTime(Date date, EDateTimeType eTimeType) {
        if (date != null) {
            LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
            if (eTimeType.equals(EDateTimeType.EOD)) {
                localDateTime = localDateTime.withHour(23).withMinute(59).withSecond(59).withNano(999);
            }
            if (eTimeType.equals(EDateTimeType.SOD)) {
                localDateTime = localDateTime.withHour(0).withMinute(0).withSecond(0).withNano(0);
            }
            return localDateTime;
        }
        return null;
    }

    public static Date convertDateFormat(Date date, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        if (date != null) {
            try {
                return dateFormat.parse(dateFormat.format(date));
            } catch (ParseException e) {
                return date;
            }
        }
        return null;
    }

    public static Date convertDate(Date date) {
        if (date == null) {
            date = new Date();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        date = calendar.getTime();
        return date;
    }

    public static String getTomorrowDate() {
        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate todays = LocalDate.now(ZoneId.of(ConstDateFormat.APP_TIME_ZONE));
        LocalDateTime todayMidnight = LocalDateTime.of(todays, midnight);
        LocalDateTime tomorrowMidnight = todayMidnight.plusDays(1);
        return convertLocalDateTimeToDate(tomorrowMidnight, ConstDateFormat.jsonDateFormat);
    }

    public static String getTomorrowDay() {
        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate todays = LocalDate.now(ZoneId.of(ConstDateFormat.APP_TIME_ZONE));
        LocalDateTime todayMidnight = LocalDateTime.of(todays, midnight);
        LocalDateTime tomorrowMidnight = todayMidnight.plusDays(1);
        return tomorrowMidnight.getDayOfWeek().toString();
    }

    public static Date convertStringToTime(String time) {
        DateFormat formatter = new SimpleDateFormat(ConstDateFormat.TIME_FORMAT_H_MM_A);
        try {
            Date date = formatter.parse(time);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    public static boolean checkSMCOfficialTime(String time) {
        if (time == null || time.isEmpty()) {
            return false;
        }
        Date smcOpeningTime = convertStringToTime(ConstDateFormat.OPENING_TIME);
        Date smcClosingTime = convertStringToTime(ConstDateFormat.CLOSING_TIME);
        Date date = convertStringToTime(time);
        if ((date.equals(smcOpeningTime) || date.equals(smcClosingTime))
                || (date.after(smcOpeningTime) && date.before(smcClosingTime))) {
            return true;
        } else {
            return false;
        }
    }

    public static String getDay(Date date) {
        if (date == null) {
            date = new Date();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String day = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);
        return day;
    }

    // To add days to the given date
    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); // minus number would decrement the days
        Date dt = cal.getTime();
        return dt;
    }

    // To add hours to the given date
    public static Date addHours(Date date, int count) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, count); // minus number would decrement the hours
        return cal.getTime();
    }

    // to find whether given date should between two dates-return-true or false
    public static boolean dateBetween(Date startDate, Date endDate, Date myDate) {
        return myDate.after(startDate) && myDate.before(endDate);
    }

    public static Date convertStringToDate(String strDate, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        try {
            return formatter.parse(strDate);
        } catch (ParseException e) {
            return null;
        }
    }

    public static boolean checkFutureDateWithSystemDate(Date futureDate) {
        if ((futureDate.after(new Date()))) {
            return true;
        }
        return false;
    }

    public static boolean checkPreviousDateWithSystemDate(Date previousDate) {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String stringdate = simpleDateFormat.format(date);
        date = TimeZoneConversionUtil.stringToDate(stringdate);
        try {

            System.out.println("date: " + date);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return (previousDate.before(date));
    }

    public static int getYear(int limit) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, limit);
        return calendar.get(Calendar.YEAR);
    }

    public static boolean dateGreaterThanEqual(Date startDate, Date myDate) {
        return myDate.after(startDate) || myDate.equals(startDate);
    }

    public static boolean dateLessThanEqual(Date endDate, Date myDate) {
        return myDate.equals(endDate) || myDate.before(endDate);
    }

    /* To compare two Date lists to get the Common Dates */
    public static List<Date> getCommonDates(List<Date> dateList1, List<Date> dateList2) {
        dateList1.retainAll(dateList2);
        return dateList1;
    }

    public static Date convertStringTimeAndDateToDate(String startTime, String date) {
        String datetime = date + " " + startTime;
        SimpleDateFormat sdf = new SimpleDateFormat(ConstDateFormat.TIME_FORMAT_DD_MM_YYYY_HH_MM_A);
        try {
            return sdf.parse(datetime);
        } catch (ParseException e) {
            return null;
        }
    }

    /* To add Months from the given date */
    public static Date addMonths(Date date, int count) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, count);
        return cal.getTime();
    }

    /* Get Month index from given date */
    public static int getMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH);
    }

    /* Get Year from given date */
    public static int getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    /* Get Month index from Month name */
    public static int getMonth(String monthName) {
        Date date;
        try {
            date = new SimpleDateFormat(ConstDateFormat.DATE_FORMAT_MMMM).parse(monthName);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return cal.get(Calendar.MONTH);
        } catch (ParseException e) {
            return 0;
        }
    }

    /* Get Date from Month & Year - it returns first date of month */
    public static Date getDate(int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }

    public static Boolean validateInBetweenDateList(Date startDate, Date endDate, List<Date> datesToCheck) {
        for (Date date : datesToCheck) {
            if ((DateConversionUtil.dateGreaterThanEqual(startDate, date)
                    && DateConversionUtil.dateLessThanEqual(endDate, date)) == false) {
                return true;
            }
        }
        return false;
    }

    public static List<String> getStringDateList(Date fromDate, String dateRange) {
        List<String> programDates = new ArrayList<>();
        programDates.add(DateConversionUtil.convertDate(fromDate, ConstDateFormat.jsonDateFormat));
        if (dateRange != null) {
            programDates.addAll(
                    Arrays.asList(dateRange.split(",")).stream().map(String::trim).collect(Collectors.toList()));
        }
        return programDates;
    }

    /* To get date list from a given date range */
    public static List<Date> getDateList(Date fromDate, String dateRange) {
        List<Date> dateList = new ArrayList<>();
        dateList.add(DateConversionUtil.convertDateFormat(fromDate, ConstDateFormat.DATE_FORMAT_DD_MM_YYYY));
        if (dateRange != null) {
            List<String> dateListStr = Arrays.asList(dateRange.split(","));
            dateList = dateListStr.stream().filter(dt -> dt != null)
                    .map(dt -> DateConversionUtil.convertStringToDate(dt, ConstDateFormat.DATE_FORMAT_DD_MM_YYYY))
                    .collect(Collectors.toList());
        }
        return dateList;
    }

    /* To get Least Date from Date List */
    public static Date getLeastDate(List<Date> dateList) {
        return dateList.stream().sorted().findFirst().orElse(null);
    }

    public static boolean dateLesserThanEqual(Date startDate, Date myDate) {
        return myDate.before(startDate) || myDate.equals(startDate);
    }

    // To add Seconds to the given count
    public static Date addSeconds(Date date, int count) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.SECOND, count); // minus number would decrement the
        // days
        return cal.getTime();
    }

    public static Date addTenDays(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date dt = new Date();
        try {
            date = format.parse(TimeZoneConversionUtil.dateToString(date));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 10);
        System.out.println("Date :" + c);
        return dt = c.getTime();
    }

    public static boolean getNextDate(String curDate) {
        final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        try {
            date = format.parse(curDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, 10);
        return format.format(calendar.getTime()) != null;
    }

    public static int getDateDiffernce(Date startDate, Date endDate, List<Date> holidayList) {
        List<Date> dateList = getDateListforGivenStartAndEndDate(startDate, endDate);
        int size = getDateSizeWithoutHolidays(dateList, holidayList);
        return size;
    }

    public static List<Date> getDateListforGivenStartAndEndDate(Date startDate, Date endDate) {
        LocalDate start = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate end = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        end = end.minusDays(1);
        List<LocalDate> totalDates = new ArrayList<>();
        while (!start.isAfter(end)) {
            totalDates.add(start);
            start = start.plusDays(1);
        }
        return totalDates.stream().map(dt -> Date.from(dt.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .collect(Collectors.toList());
    }

    public static int getDateSizeWithoutHolidays(List<Date> dates, List<Date> holidayList) {
        LocalDateTime endDate = dates.stream().sorted().map(dt -> DateConversionUtil.convertDateToLocalDateTime(dt))
                .findFirst().orElse(null);
        int daysCount = 0;
        List<LocalDateTime> holidays = holidayList.stream()
                .map(date -> DateConversionUtil.convertDateToLocalDateTime(date)).collect(Collectors.toList());
        List<LocalDateTime> holiDaysSOD = holidays.stream().map(d -> d.toLocalDate().atStartOfDay())
                .collect(Collectors.toList());
        List<Date> dateSize = new ArrayList<>();
        while (daysCount < dates.size()) {
            if ((endDate.getDayOfWeek() == DayOfWeek.SATURDAY || endDate.getDayOfWeek() == DayOfWeek.SUNDAY) == false) {
                if (holiDaysSOD.contains(endDate.toLocalDate().atStartOfDay()) == false) {
                    dateSize.add(DateConversionUtil.convertLocalDateTimeToDate(endDate));
                }
            }
            endDate = endDate.plusDays(1);
            daysCount++;
        }
        return dateSize.size();
    }

    public static int getDateDiffernceWithHolidays(Date startDate, Date endDate) {
        long diff = endDate.getTime() - startDate.getTime();
        return (int) (diff / (24 * 60 * 60 * 1000));

    }

    public static boolean isWeekEndDay(Date date) {
        LocalDateTime localDateTime = convertDateToLocalDateTime(date);
        if (localDateTime.getDayOfWeek().equals(DayOfWeek.SATURDAY)
                || localDateTime.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
            return true;
        }
        return false;
    }

    public static Date getDateAfterTimeInterval(LocalDateTime localDateTime, int timeInterval,
            int weekEndTimeInterval) {
        Date date = convertLocalDateTimeToDate(localDateTime);
        if (localDateTime.getDayOfWeek().equals(DayOfWeek.FRIDAY)) {
            date = addHours(date, weekEndTimeInterval);
        } else {
            date = addHours(date, timeInterval);
        }
        return date;
    }

    /* To check if endDate is after startDate or not */
    public static boolean isAfter(Date startDate, Date endDate) {
        return endDate.after(startDate);
    }

    public static LocalDateTime getAddDays(LocalDateTime date, int limit) {
        return date.plusDays(limit);
    }

    public static LocalDateTime getAddHours(LocalDateTime date, int limit) {
        return date.plusHours(limit);
    }

    public static LocalDateTime getAddHoursAndMins(LocalDateTime date, int hourlimit, int minLimit) {
        return date.plusHours(hourlimit).plusMinutes(minLimit);
    }

    public static boolean isAfter(LocalDateTime dueDate) {
        return LocalDateTime.now().isAfter(dueDate);
    }

    public static boolean getIsWeekEndDay(LocalDateTime localDateTime) {
        if (localDateTime.getDayOfWeek().equals(DayOfWeek.SATURDAY)
                || localDateTime.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
            return true;
        } else {
            return false;
        }
    }

    public static LocalDateTime getAddWeeks(LocalDateTime date, int limit) {
        return date.plusWeeks(limit);
    }

    public static LocalDateTime getAddMonth(LocalDateTime date, int limit) {
        return date.plusMonths(limit);
    }

    public static boolean dateBetween(LocalDateTime startDate, LocalDateTime endDate, LocalDateTime myDate) {
        return myDate.isAfter(startDate) && myDate.isBefore(endDate);
    }

    public static Date convertStringToDateTime(String dateTime) {
        DateFormat formatter = new SimpleDateFormat(ConstDateFormat.DATE_FORMAT_DD_MM_YYYY_HH_MM_AM_PM);
        try {
            Date date = formatter.parse(dateTime);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    public static Long numberOfDays(Date startDate, String endDate) {
        List<String> stringDateList = DateConversionUtil.getStringDateList(startDate, endDate);
        List<Date> dateList = stringDateList.stream()
                .map(str -> DateConversionUtil.convertStringToDate(str, ConstDateFormat.jsonDateFormat))
                .collect(Collectors.toList()).stream().sorted().collect(Collectors.toList());
        Date start = dateList.stream().findFirst().orElse(null);
        Date end = dateList.stream().reduce((first, second) -> second).orElse(null);
        if (end != null) {
            return TimeUnit.DAYS.convert((end.getTime() - start.getTime()), TimeUnit.MILLISECONDS) + 1;
        } else {
            return 1L;
        }
    }

    /* To add Year from the given date */
    public static Date addYears(Date date, int count) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, count);
        return cal.getTime();
    }

    public static int monthsBetweenDates(Date startDate, Date endDate) {

        Calendar start = Calendar.getInstance();
        start.setTime(startDate);

        Calendar end = Calendar.getInstance();
        end.setTime(endDate);

        int monthsBetween = 0;
        int dateDiff = end.get(Calendar.DAY_OF_MONTH) - start.get(Calendar.DAY_OF_MONTH);

        if (dateDiff < 0) {
            int borrow = end.getActualMaximum(Calendar.DAY_OF_MONTH);
            dateDiff = (end.get(Calendar.DAY_OF_MONTH) + borrow) - start.get(Calendar.DAY_OF_MONTH);
            monthsBetween--;

            if (dateDiff > 0) {
                monthsBetween++;
            }
        } else {
            monthsBetween++;
        }
        monthsBetween += end.get(Calendar.MONTH) - start.get(Calendar.MONTH);
        monthsBetween += (end.get(Calendar.YEAR) - start.get(Calendar.YEAR)) * 12;
        return monthsBetween;
    }

    /* Get the first or last date of given month & year */
    public static Date getDateOfMonth(int year, int month, EDateTimeType eTimeType) {
        LocalDateTime localDateTime = null;
        if (eTimeType.equals(EDateTimeType.SOM)) {
            localDateTime = LocalDateTime.of(year, month, 1, 0, 0, 0);
        }
        if (eTimeType.equals(EDateTimeType.EOM)) {
            YearMonth yearMonth = YearMonth.of(year, month);
            localDateTime = LocalDateTime.of(year, month, yearMonth.lengthOfMonth(), 23, 59, 59);
        }
        return convertLocalDateTimeToDate(localDateTime);
    }

    public static List<Date> getDateRanges(String dateRange) {
        List<Date> dateRanges = new ArrayList<Date>(0);
        if (Validation.isNotEmpty(dateRange)) {
            String[] dateStrArr = dateRange.split("-");
            int index = 0;
            for (String str : dateStrArr) {
                Date date = TimeZoneConversionUtil.stringToDate(str.trim(), ConstDateFormat.DATE_FORMAT_MMM_YYYY1);
                EDateTimeType dateTimeType = null;
                if (index == 0) {
                    dateTimeType = EDateTimeType.SOM;
                } else {
                    dateTimeType = EDateTimeType.EOM;
                }
                date = getDateOfMonth(getYear(date), getMonth(date) + 1, dateTimeType);
                dateRanges.add(date);
                index++;
            }
        }
        return dateRanges;
    }

    public static String getGMTTimeStampToTime(String GMTrequest) {
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd yyyy hh:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("IST"));
        Date date = null;
        try {
            date = format.parse(GMTrequest);
        } catch (ParseException e) {
            ExceptionUtil.throwException(EExceptionType.INVALID_DATA, "Invalid Date Format");
        }
        DateFormat time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return time.format(date);
    }

    public static String getUTCTimeStampToTime(String UTCrequest) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = format.parse(UTCrequest);
        } catch (ParseException e) {
            ExceptionUtil.throwException(EExceptionType.INVALID_DATA, "Invalid Date Format");
        }
        DateFormat time = new SimpleDateFormat("hh:mm a");
        return time.format(date);
    }

    public static Date addDuration(Date date, int expirationTime, int calendar) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(calendar, expirationTime);
        return cal.getTime();
    }
}
