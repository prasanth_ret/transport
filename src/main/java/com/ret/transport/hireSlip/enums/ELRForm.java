package com.ret.transport.hireSlip.enums;

public enum ELRForm {

    CONSIGNOR(1L, "Consignor Copy"),
    CONSIGNEE(2L, "Consignee Copy"),
    OTHER(3L, "Other Copy"),
    EXTRA(4L, "Extra Copy"),
    TRUCK_COPY(5L,"Truck Copy");

    private Long id;
    private String value;

    ELRForm(Long id, String value) {
        this.value = value;
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public Long getId() {
        return id;
    }

}
