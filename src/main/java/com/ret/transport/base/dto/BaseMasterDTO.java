/**
 * 
 */
package com.ret.transport.base.dto;

/**
 * @author Vishnu on 03-Oct-2017
 */
public class BaseMasterDTO extends BaseDTO {

	private static final long serialVersionUID = -2137140635264519807L;

	private Long id;
	private String name;
	private String description;
	private String enumName;
	private String label;
	private Long value;

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEnumName() {
		return enumName;
	}

	public void setEnumName(String enumName) {
		this.enumName = enumName;
	}

}
