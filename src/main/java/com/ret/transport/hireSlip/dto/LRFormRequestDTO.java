package com.ret.transport.hireSlip.dto;

import com.ret.transport.base.search.SearchParam;

public class LRFormRequestDTO extends SearchParam {

    private static final long serialVersionUID = 1L;

    /* Base Details */
    private Long id;
    private Long loginId;

    /* LR Form Request Parameter */
    private String lrNumber;
    private String consignor;
    private String consignee;
    private Double statisticalCharges;
    private Double cgst;
    private Double sgst;
    private Double igst;
    private Double total;
    private Double valueRs;
    private Double unloadingCharges;
    private Double ratePerBox;
    private Double freightToPay;
    private String date;
    private Long noOfPackages;
    private Double actualKgs;
    private Double chargedKgs;

    public String getLrNumber() {
        return lrNumber;
    }

    public void setLrNumber(String lrNumber) {
        this.lrNumber = lrNumber;
    }

    public String getConsignor() {
        return consignor;
    }

    public void setConsignor(String consignor) {
        this.consignor = consignor;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public Double getStatisticalCharges() {
        return statisticalCharges;
    }

    public void setStatisticalCharges(Double statisticalCharges) {
        this.statisticalCharges = statisticalCharges;
    }

    public Double getCgst() {
        return cgst;
    }

    public void setCgst(Double cgst) {
        this.cgst = cgst;
    }

    public Double getSgst() {
        return sgst;
    }

    public void setSgst(Double sgst) {
        this.sgst = sgst;
    }

    public Double getIgst() {
        return igst;
    }

    public void setIgst(Double igst) {
        this.igst = igst;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getValueRs() {
        return valueRs;
    }

    public void setValueRs(Double valueRs) {
        this.valueRs = valueRs;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getNoOfPackages() {
        return noOfPackages;
    }

    public void setNoOfPackages(Long noOfPackages) {
        this.noOfPackages = noOfPackages;
    }

    public Double getActualKgs() {
        return actualKgs;
    }

    public void setActualKgs(Double actualKgs) {
        this.actualKgs = actualKgs;
    }

    public Double getChargedKgs() {
        return chargedKgs;
    }

    public void setChargedKgs(Double chargedKgs) {
        this.chargedKgs = chargedKgs;
    }

    public Double getFreightToPay() {
        return freightToPay;
    }

    public void setFreightToPay(Double freightToPay) {
        this.freightToPay = freightToPay;
    }

    public Double getUnloadingCharges() {
        return unloadingCharges;
    }

    public void setUnloadingCharges(Double unloadingCharges) {
        this.unloadingCharges = unloadingCharges;
    }

    public Double getRatePerBox() {
        return ratePerBox;
    }

    public void setRatePerBox(Double ratePerBox) {
        this.ratePerBox = ratePerBox;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

}
