package com.ret.transport.hireSlip.enums;

public enum EPaymentStatus {

    PAID("Paid"),
    PENDING("Pending");

    private String value;

    EPaymentStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
