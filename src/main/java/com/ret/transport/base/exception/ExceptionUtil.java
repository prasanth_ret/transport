package com.ret.transport.base.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.jwt.service.JWTTokenExpiredException;
import com.ret.transport.base.exception.jwt.service.JWTTokenMalformedException;
import com.ret.transport.base.exception.jwt.service.JWTTokenMissingException;
import com.ret.transport.base.exception.jwt.service.UnauthorizedException;
import com.ret.transport.base.exception.service.BadRequestException;
import com.ret.transport.base.exception.service.DataAlreadyExistsException;
import com.ret.transport.base.exception.service.DataNotFoundException;
import com.ret.transport.base.exception.service.DocumentNotFoundException;
import com.ret.transport.base.exception.service.InvalidDataException;

/**
 * Vishnu on 07-Feb-2018
 */
public class ExceptionUtil {

    private static final Logger logger = LogManager.getLogger(ExceptionUtil.class);

    public static void throwDataNotFoundException(String exceptionMsg) {
        DataNotFoundException exception = new DataNotFoundException(exceptionMsg);
        logger.error(exceptionMsg, exception);
        throw exception;
    }

    public static void throwDataAlreadyExistsException(String exceptionMsg) {
        DataAlreadyExistsException exception = new DataAlreadyExistsException(exceptionMsg);
        logger.error(exceptionMsg, exception);
        throw exception;
    }

    public static void throwInvalidDataException(String exceptionMsg) {
        InvalidDataException exception = new InvalidDataException(exceptionMsg);
        logger.error(exceptionMsg, exception);
        throw exception;
    }

    public static void throwDocumentNotFoundException(String exceptionMsg) {
        DocumentNotFoundException exception = new DocumentNotFoundException(exceptionMsg);
        logger.error(exceptionMsg, exception);
        throw exception;
    }

    public static void throwUnauthorizedException(String exceptionMsg) {
        UnauthorizedException exception = new UnauthorizedException(exceptionMsg);
        logger.error(exceptionMsg, exception);
        throw exception;
    }

    public static void throwJWTTokenExpiredException(String exceptionMsg) {
        JWTTokenExpiredException exception = new JWTTokenExpiredException(exceptionMsg);
        logger.error(exceptionMsg, exception);
        throw exception;
    }

    public static void throwJWTTokenMalformedException(String exceptionMsg) {
        JWTTokenMalformedException exception = new JWTTokenMalformedException(exceptionMsg);
        logger.error(exceptionMsg, exception);
        throw exception;
    }

    public static void throwJWTTokenMissingException(String exceptionMsg) {
        JWTTokenMissingException exception = new JWTTokenMissingException(exceptionMsg);
        logger.error(exceptionMsg, exception);
        throw exception;
    }

    private static void throwBadRequestException(String exceptionMsg) {
        BadRequestException exception = new BadRequestException(exceptionMsg);
        logger.error(exceptionMsg, exception);
        throw exception;
    }

    public static void throwException(EExceptionType exceptionType, String message) {
        switch (exceptionType) {
        case DATA_NOT_FOUND:
            throwDataNotFoundException(message);
            break;
        case DATA_ALREADY_EXISTS:
            throwDataAlreadyExistsException(message);
            break;
        case INVALID_DATA:
            throwInvalidDataException(message);
            break;
        case DOCUMENT_NOT_FOUND:
            throwDocumentNotFoundException(message);
            break;
        case UNAUTHORIZED:
            throwUnauthorizedException(message);
            break;
        case JWT_TOKEN_EXPIRED:
            throwJWTTokenExpiredException(message);
            break;
        case JWT_TOKEN_MALFORMED:
            throwJWTTokenMalformedException(message);
            break;
        case JWT_TOKEN_MISSING:
            throwJWTTokenMissingException(message);
        case BAD_REQUEST:
            throwBadRequestException(message);
        default:
            throwInvalidDataException("Invalid Exception Type");
            break;
        }
    }

}
