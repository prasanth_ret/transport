package com.ret.transport.base.search.enums;

/**
 * @author Vishnu on 01-Oct-2017
 */
public enum ESearchDirection {
    ASC("asc"), DESC("desc");

    private String value;

    ESearchDirection(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
