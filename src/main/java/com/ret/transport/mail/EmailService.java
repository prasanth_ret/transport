package com.ret.transport.mail;

import java.util.List;

public interface EmailService {

    void sendEmail(List<String> recipients, List<String> ccRecipients, String subject, String text,
            String... attachments);

}
