package com.ret.transport.base.exception.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class DataNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 3481889740944533624L;

    private static final Log LOG = LogFactory.getLog(DataNotFoundException.class);

    /**
     * @param message
     * @param cause
     */
    public DataNotFoundException(String message, Throwable cause) {
        super(message, cause);
        LOG.error(message);
    }

    /**
     * @param message
     */
    public DataNotFoundException(String message) {
        super(message);
        LOG.error(message);
    }

    /**
     * @param cause
     */
    public DataNotFoundException(Throwable cause) {
        super(cause);

    }

}
