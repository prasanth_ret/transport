package com.ret.transport.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * @author Vishnu on 30-Jan-2018
 */
@Component
public class JWTAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = 3798723588865329956L;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException authException) throws IOException {
        // This is invoked when user tries to access a secured REST resource without supplying any
        // credentials
        // We should just send a 401 Unauthorized response because there is no 'login page' to
        // redirect to
        // response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
        ExceptionUtil.throwException(EExceptionType.UNAUTHORIZED, "Unauthorized User");
    }
}
