/**
 * 
 */
package com.ret.transport.config;

import java.io.Serializable;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class WebAppUtil implements Serializable {

    private static final long serialVersionUID = -635950367023309592L;

    public static String APPLICATION_PROPERTY = "/application.properties";
    public static String DEFAULT_ACTIVE_PROFILE = "default";

    public static String LOCATION = "/tmp";
    public static Long MAX_FILE_SIZE = 1024l * 1024l * 5l;
    public static Long MAX_REQUEST_SIZE = MAX_FILE_SIZE * 5l;
    public static int FILE_SIZE_THRESHOLD = 1024 * 1024;
    public static String DEFAULT_ENCODING = "utf-8";

    public static String BASE_PACKAGE = "com.ret";

    public static String URL_PREFIX = "/jsp/";
    public static String URL_SUFFIX = ".jsp";
}
