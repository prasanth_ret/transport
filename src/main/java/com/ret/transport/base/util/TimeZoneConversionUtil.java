package com.ret.transport.base.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class TimeZoneConversionUtil implements ConstDateFormat {

    /* To Convert date to String with System Timezone */
    public static String dateToString(Date date) {
        if (date != null) {
            DateFormat dateFormat = new SimpleDateFormat(jsonDateFormat);
            dateFormat.setTimeZone(TimeZone.getDefault());
            return dateFormat.format(date);
        }
        return null;
    }

    /* To Convert date to given TimeZone */
    public static String convertDate(Date date, String timeZone) {
        if (date != null) {
            DateFormat dateFormat = new SimpleDateFormat(jsonDateFormat);
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            return dateFormat.format(date);
        }
        return null;
    }

    /* To Convert String to Date with System Timezone */
    public static Date stringToDate(String strDate) {
        if (Validation.isNotEmpty(strDate)) {
            DateFormat dateFormat = new SimpleDateFormat(jsonDateFormat);
            dateFormat.setTimeZone(TimeZone.getTimeZone(APP_TIME_ZONE));
            try {
                return dateFormat.parse(strDate);
            } catch (ParseException e) {
                ExceptionUtil.throwException(EExceptionType.INVALID_DATA, "Invalid Date");
            }
        }
        return null;
    }

    /* To Convert String to Date with System Timezone */
    public static Date stringToDate(String strDate, String format) {
        if (Validation.isNotEmpty(strDate) && Validation.isNotEmpty(format)) {
            DateFormat dateFormat = new SimpleDateFormat(format);
            dateFormat.setTimeZone(TimeZone.getTimeZone(APP_TIME_ZONE));
            try {
                return dateFormat.parse(strDate);
            } catch (ParseException e) {
                ExceptionUtil.throwException(EExceptionType.INVALID_DATA, "Invalid Date");
            }
        }
        return null;
    }

    /* To Convert String to LocalDateTime with System Timezone */
    public static LocalDateTime getLocalDateTime() {
        return LocalDateTime.ofInstant(new Date().toInstant(), ZoneId.of(APP_TIME_ZONE));
    }

    public static LocalDateTime dateToLocalDateTime(Date date) {
        if (Validation.isNotNull(date)) {
            return LocalDateTime.ofInstant(date.toInstant(), ZoneId.of(APP_TIME_ZONE));
        }
        return null;
    }
}
