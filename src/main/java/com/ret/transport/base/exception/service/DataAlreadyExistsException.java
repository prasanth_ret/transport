package com.ret.transport.base.exception.service;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class DataAlreadyExistsException extends RuntimeException {
    private static final long serialVersionUID = 7137678873286271728L;

    public DataAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataAlreadyExistsException(String message) {
        super(message);
    }

    public DataAlreadyExistsException(Throwable cause) {
        super(cause);
    }
}
