GO

---------------------
-- SMC DB Common Schema --
---------------------

CREATE SCHEMA dbcommon;
GO


--
-- Roles
--
CREATE TABLE dbcommon.tb_role (
    id BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(100),
    status BIT NOT NULL DEFAULT 1,
    created_by BIGINT DEFAULT 0,
    created_at DATETIME DEFAULT GETDATE(),
    modified_by BIGINT DEFAULT 0,
    modified_at DATETIME DEFAULT GETDATE()
);
GO
--
-- Address
--
CREATE TABLE dbcommon.tb_address (
    id BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
    address VARCHAR(250),
    city VARCHAR(100),
    pin_code VARCHAR(10),
    country_code VARCHAR(5),
    country VARCHAR(50),
    phone_number VARCHAR(15),
    mobile_number VARCHAR(12),
    fax_number VARCHAR(12),
    status BIT NOT NULL DEFAULT 1,
    created_by BIGINT DEFAULT 0,
    created_at DATETIME DEFAULT GETDATE(),
    modified_by BIGINT DEFAULT 0,
    modified_at DATETIME DEFAULT GETDATE()
);
GO


--
-- User
--
CREATE TABLE dbcommon.tb_user (
   id BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
   first_name VARCHAR(50) NOT NULL,
   last_name VARCHAR(50),
   email VARCHAR(100),
   pan_no VARCHAR(100),
   phone_number VARCHAR(50),
   alternate_number1 VARCHAR(50) ,
   alternate_number2 VARCHAR(50),
   password VARCHAR(100),
   dob DATE,
   license_number VARCHAR(100),
   address_id BIGINT,
   is_login_available BIT NOT NULL,
   status BIT NOT NULL DEFAULT 1,
   created_by BIGINT DEFAULT 0,
   created_at DATETIME DEFAULT GETDATE(),
   modified_by BIGINT DEFAULT 0,
   modified_at DATETIME DEFAULT GETDATE(),
   CONSTRAINT fk_user_address FOREIGN KEY (address_id) REFERENCES dbcommon.tb_address (id) 
);
GO

--
-- User Role Mapping
--
CREATE TABLE dbcommon.tb_user_role_map (
    id BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
    user_id BIGINT,
    role_id BIGINT NOT NULL,
    account_status VARCHAR(50),
    status BIT NOT NULL DEFAULT 1,
    created_by BIGINT DEFAULT 0,
    created_at DATETIME DEFAULT GETDATE(),
    modified_by BIGINT DEFAULT 0,
    modified_at DATETIME DEFAULT GETDATE(),
    CONSTRAINT fk_user_map FOREIGN KEY (user_id) REFERENCES dbcommon.tb_user (id),
    CONSTRAINT fk_role_map FOREIGN KEY (role_id) REFERENCES dbcommon.tb_role (id)
);
GO
--
-- LR Form
--
CREATE TABLE dbcommon.tb_lr_form(
	id BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
	lr_number VARCHAR(50),
	consignor VARCHAR(250),
	consignee VARCHAR(250),
	rate_per_box DECIMAL(15,2),
	statistical_charges DECIMAL(15,2),
	cgst DECIMAL(15,2),
	sgst DECIMAL(15,2),
	igst DECIMAL(15,2),
	total DECIMAL(15,2),
	value_rs DECIMAL(15,2),
	actual_kgs DECIMAL(15,2),
	charged_kgs DECIMAL(15,2),
	freight_to_pay DECIMAL(15,2),
	unloading_charges DECIMAL(15,2),
	date DATE,
	bill_no VARCHAR(50),
	bill_date DATE,
	status BIT NOT NULL DEFAULT 1,
	created_by BIGINT DEFAULT 0,
	created_at DATETIME DEFAULT GETDATE(),
	modified_by BIGINT DEFAULT 0,
	modified_at DATETIME DEFAULT GETDATE()
);
GO