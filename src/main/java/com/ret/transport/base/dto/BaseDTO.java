package com.ret.transport.base.dto;

import java.io.Serializable;

/**
 * All response DTO classes should extend this class
 *
 * @author Vishnu on 01-Oct-2017
 */
public abstract class BaseDTO implements Serializable {

    private static final long serialVersionUID = 3742108370741083705L;

    private Long id;
    private Long loginId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

}
