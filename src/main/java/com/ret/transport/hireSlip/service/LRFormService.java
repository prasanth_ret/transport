package com.ret.transport.hireSlip.service;

import org.springframework.data.domain.Page;

import com.ret.transport.hireSlip.dto.LRFormRequestDTO;
import com.ret.transport.hireSlip.dto.LRFormResponseDTO;
import com.ret.transport.hireSlip.entity.LRForm;

public interface LRFormService {

    Page<LRForm> getLRFormList(LRFormRequestDTO requestDTO);

    LRForm saveLrForm(LRForm lrForm);

    void getDeleteLRForm(Long id, Long modifiedBy);

    LRForm findLRForm(Long id);

    LRForm saveOrUpdateLRForm(LRFormRequestDTO dto, Long modifiedBy);

    LRForm getLRForm(Long id);

}
