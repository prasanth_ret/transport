package com.ret.transport.base.util;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ret.transport.base.exception.service.InvalidDataException;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class PasswordUtil {

    /* Password Characters */
    private static final String DIGITS = "1234567890";
    private static final String LOWER_CASE_CHARS = "abcdefghijklmnopqrstuvwxyz";
    public static final String UPPER_CASE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String SPECIAL_CHARS = "@#$%=:?";
    private static final String ALL = String.format("%s%s%s%s", UPPER_CASE_CHARS, LOWER_CASE_CHARS, DIGITS,
            SPECIAL_CHARS);

    /*
     * To Generate a Random Password based on SMC Password Policy. The password will consist of at
     * least 1 Capital, 1 Small Alphabet, 1 Number and 1 special character
     */
    public static String generatePassword(int minLength) {
        if (minLength == 0) {
            throw new InvalidDataException("Invalid Password Minimum Length");
        }
        /* Password Character Array */
        final char[] upperCaseArr = UPPER_CASE_CHARS.toCharArray();
        final char[] lowerCaseArr = LOWER_CASE_CHARS.toCharArray();
        final char[] digitsArr = DIGITS.toCharArray();
        final char[] specialCharArr = SPECIAL_CHARS.toCharArray();
        final char[] allCharArr = ALL.toCharArray();

        StringBuilder pwdBuilder = new StringBuilder();

        /* The random number generator. */
        Random random = new Random();

        /* To get at least one Lower Case Letter */
        pwdBuilder.append(lowerCaseArr[random.nextInt(lowerCaseArr.length)]);

        /* To get at least one Upper Case Letter */
        pwdBuilder.append(upperCaseArr[random.nextInt(upperCaseArr.length)]);

        /* To get at least one Digit */
        pwdBuilder.append(digitsArr[random.nextInt(digitsArr.length)]);

        /* To get at least one Special Character */
        pwdBuilder.append(specialCharArr[random.nextInt(specialCharArr.length)]);
        int length = pwdBuilder.toString().length();

        /* To fill in remaining with random Password Characters */
        for (int i = 0; i < minLength - length; i++) {
            pwdBuilder.append(allCharArr[random.nextInt(allCharArr.length)]);
        }
        return pwdBuilder.toString();
    }

    /* To Validate the generated/given password is match with SMC Password Policy or not */
    public static boolean validatePassword(final String password, int minLength, int maxLength) {
        if (minLength == 0) {
            throw new InvalidDataException("Invalid Password Minimum Length");
        }
        if (maxLength == 0) {
            throw new InvalidDataException("Invalid Password Maximum Length");
        }
        final String PASSWORD_PATTERN = String.format("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[%s]).{%s,%s})",
                SPECIAL_CHARS, minLength, maxLength);
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

}
