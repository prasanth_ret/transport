GO
/****** Object:  Table [dbcommon].[tb_role]    Script Date: 05/16/2018 16:53:04 ******/
SET IDENTITY_INSERT [dbcommon].[tb_role] ON
INSERT [dbcommon].[tb_role] ([id], [name], [description], [status], [created_by], [created_at], [modified_by], [modified_at]) VALUES (1, N'Super Admin', N'Super Admin', 1, 0, CAST(0x0000A8DA01409933 AS DateTime), 0, CAST(0x0000A8DA01409933 AS DateTime))
INSERT [dbcommon].[tb_role] ([id], [name], [description], [status], [created_by], [created_at], [modified_by], [modified_at]) VALUES (2, N'Admin', N'Admin', 1, 0, CAST(0x0000A8DA01409933 AS DateTime), 0, CAST(0x0000A8DA01409933 AS DateTime))
SET IDENTITY_INSERT [dbcommon].[tb_role] OFF
/****** Object:  Table [dbcommon].[tb_lr_number]    Script Date: 05/16/2018 16:53:04 ******/
SET IDENTITY_INSERT [dbcommon].[tb_address] ON
INSERT [dbcommon].[tb_address] ([id], [address], [city_id], [pin_code], [country_code], [country], [phone_number], [mobile_number], [fax_number], [status], [created_by], [created_at], [modified_by], [modified_at]) VALUES (1, N'Chinniyampalayam', NULL, N'641062', N'225', N'India', N'7397181128', N'25221556', N'6555', 1, 0, CAST(0x0000A8DA01409969 AS DateTime), 0, CAST(0x0000A8DA01409969 AS DateTime))

/****** Object:  Table [dbcommon].[tb_user]    Script Date: 05/16/2018 16:53:04 ******/
SET IDENTITY_INSERT [dbcommon].[tb_user] ON
INSERT [dbcommon].[tb_user] ([id], [first_name], [last_name], [email], [pan_no], [phone_number], [password], [dob], [license_number], [address_id], [is_login_available], [status], [created_by], [created_at], [modified_by], [modified_at]) VALUES (1, N'Super Admin', NULL, N'superadmin@example.com', N'BQPPS0878', N'9874563210', N'$2a$10$Fu6H7wTcFQRtB4.YIFsbwuevoFczMl2LjVe.RGNFu7oqIJATu1iOm', CAST(0x8D1B0B00 AS Date), N'1457320R', 1, 1, 1, 0, CAST(0x0000A8DA01409969 AS DateTime), 0, CAST(0x0000A8DA01409969 AS DateTime))
SET IDENTITY_INSERT [dbcommon].[tb_user] OFF
/****** Object:  Table [dbcommon].[tb_user_role_map]    Script Date: 05/16/2018 16:53:04 ******/
SET IDENTITY_INSERT [dbcommon].[tb_user_role_map] ON
INSERT [dbcommon].[tb_user_role_map] ([id], [user_id], [role_id], [account_status], [status], [created_by], [created_at], [modified_by], [modified_at]) VALUES (1, 1, 1, N'Active', 1, 0, CAST(0x0000A8DA01409969 AS DateTime), 0, CAST(0x0000A8DA01409969 AS DateTime))
SET IDENTITY_INSERT [dbcommon].[tb_user_role_map] OFF
