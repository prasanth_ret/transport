package com.ret.transport.base.exception.jwt.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.AuthenticationException;

/**
 * @author Vishnu on 30-Jan-2018
 */
public class JWTTokenExpiredException extends AuthenticationException {
    private static final long serialVersionUID = 2031004709714222241L;

    private static final Log LOG = LogFactory.getLog(JWTTokenExpiredException.class);

    public JWTTokenExpiredException(String message, Throwable cause) {
        super(message, cause);
        LOG.error(message);
    }

    public JWTTokenExpiredException(String message) {
        super(message);
        LOG.error(message);
    }

}
