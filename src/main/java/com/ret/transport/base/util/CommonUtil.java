package com.ret.transport.base.util;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class CommonUtil {

    public static String BLANK_THERATER_SPACE = "X";
    public static String THERATER_SPLITTER = "_";
    public static String COMMA = ",";
    public static String HASH = "#";

    public static String getStringWithSperator(List<String> strs, String sperator) {
        return StringUtils.join(strs, sperator);
    }

    public static String generateToken() {
        return UUID.randomUUID().toString();
    }

}
