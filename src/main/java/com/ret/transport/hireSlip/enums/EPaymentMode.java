package com.ret.transport.hireSlip.enums;

import java.util.HashMap;
import java.util.Map;

public enum EPaymentMode {

    CASH(1L, "Cash"),
    CHEQUE(2L, "Cheque"),
    NEFT(3L, "Neft");

    private Long id;
    private String value;

    EPaymentMode(Long id, String value) {
        this.value = value;
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public Long getId() {
        return id;
    }

    private static final Map<String, EPaymentMode> payment = new HashMap<String, EPaymentMode>();
    static {
        for (EPaymentMode pay : values())
            payment.put(pay.getValue(), pay);
    }

    public static EPaymentMode getEnum(String role) {
        return payment.get(role);
    }

    public static String getEnum(Long id) {
        switch (id.intValue()) {
        case 1:
            return CASH.getValue();
        case 2:
            return CHEQUE.getValue();
        case 3:
            return NEFT.getValue();
        default:
            return null;
        }
    }

}
