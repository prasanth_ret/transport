package com.ret.transport.mail;

import java.io.File;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ret.transport.base.util.Validation;

@Service("emailService")
@Transactional(readOnly = false)
public class EmailServiceImpl implements EmailService {
    private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    public JavaMailSender emailSender;

    @Override
    public void sendEmail(List<String> recipients, List<String> ccRecipients, String subject, String text,
            String... attachments) {
        try {
            MimeMessage mimeMessage = emailSender.createMimeMessage();
            // pass 'true' to the constructor to create a multipart message
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            InternetAddress[] recipientsArr = new InternetAddress[recipients.size()];
            for (int index = 0; index < recipients.size(); index++) {
                recipientsArr[index] = new InternetAddress(recipients.get(index));
            }
            mimeMessageHelper.setTo(recipientsArr);

            if (Validation.isNotEmpty(ccRecipients)) {
                InternetAddress[] ccRecipientsArr = new InternetAddress[ccRecipients.size()];
                for (int index = 0; index < ccRecipients.size(); index++) {
                    ccRecipientsArr[index] = new InternetAddress(ccRecipients.get(index));
                }
                mimeMessageHelper.setCc(ccRecipientsArr);
            }
            mimeMessageHelper.setSubject(subject);
            mimeMessageHelper.setText(text);
            // mimeMessageHelper.setFrom(new InternetAddress(emailSender., personal));
            System.out.println(emailSender.toString());
            if (Validation.isNotNull(attachments)) {
                for (String attachment : attachments) {
                    FileSystemResource file = new FileSystemResource(new File(attachment));
                    mimeMessageHelper.addAttachment(file.getFilename(), file);
                }
            }
            emailSender.send(mimeMessage);
        } catch (MessagingException e) {
            logger.error(subject + " ## " + e.getMessage());

            System.out.println("Mail Failure" + e.getMessage());
        }
    }
}
