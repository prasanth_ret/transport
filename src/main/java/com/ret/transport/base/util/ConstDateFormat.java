package com.ret.transport.base.util;

/**
 * @author Vishnu on 01-Oct-2017
 */
public interface ConstDateFormat {

    /* DATE FORMATS */
    String DATE_FORMAT_DD_MMM_YYYY = "dd MMM yyyy"; // 01 Jan 2016
    String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd"; // 2016-01-01
    String DATE_FORMAT_DDMMYYYY = "ddMMMyyyy"; // 01Jan2016
    String DATE_FORMAT_DD_MM_YYYY = "dd-MM-yyyy"; // 01-01-2016
    String DATE_FORMAT_DD_MM_YYYY_HH_MM_SS_AM_PM = "dd-MM-yyyy hh:mm:ss a"; // 01-01-2016 09:30:45
                                                                            // AM
    String TRAINING_DATE_FORMAT_DD_MM_YYYY = "dd/MM/yyyy"; // 01/01/2016
    String DATE_FORMAT_DDMMYY = "ddMMMyy"; // 01Jan16
    String DATE_FORMAT_DD_MMMM_YYYY = "dd MMMM yyyy"; // 21 March 2017
    String DATE_FORMAT_MMMM_YYYY = "MMMM-yyyy"; // March-2017
    String DATE_FORMAT_MMM_YYYY = "MMM-yyyy"; // Mar-2017
    String DATE_FORMAT_MMMM = "MMMM"; // March
    String DATE_FORMAT_DD_MM_YYYY_HH_MM_AM_PM = "dd-MM-yyyy hh:mm a"; // 01-01-2016 06:00 PM
    String DATE_FORMAT_MMM_YYYY1 = "MMM/yyyy"; // Mar/2017

    String jsonDateFormat = DATE_FORMAT_DD_MM_YYYY;

    /* TIME FORMATS */
    String TIME_FORMAT_HH_MM_SS_A = "hh:mm:ss a"; // 5:04:22 PM
    String TIME_FORMAT_H_MM_SS_A = "h:mm:ss a"; // 5:04:22 PM
    String TIME_FORMAT_H_MM_A = "h:mm a"; // 5:04 PM

    /* TIME ZONES */
    String ASIA_KOLKATA = "Asia/Kolkata"; // GMT+5:30
    String ASIA_SINGAPORE = "Asia/Singapore"; // GMT+8:00

    String APP_TIME_ZONE = ASIA_KOLKATA;

    /* SMC TIMING */
    String OPENING_TIME = "9:30 AM";
    String CLOSING_TIME = "6:00 PM";

    /* DATE TIME FORMATS */
    String TIME_FORMAT_DD_MM_YYYY_HH_MM_A = "dd-MM-yyyy hh:mm a";
}
