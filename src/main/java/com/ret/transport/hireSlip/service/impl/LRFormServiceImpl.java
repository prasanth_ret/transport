package com.ret.transport.hireSlip.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;
import com.ret.transport.base.search.SearchCriteria;
import com.ret.transport.base.search.SearchSpecification;
import com.ret.transport.base.search.enums.ECondition;
import com.ret.transport.base.search.enums.ESearchOperator;
import com.ret.transport.base.search.util.SearchSpecificationUtil;
import com.ret.transport.base.util.ConstDateFormat;
import com.ret.transport.base.util.DateConversionUtil;
import com.ret.transport.base.util.Validation;
import com.ret.transport.hireSlip.dto.CustomerInfoMessageUtil;
import com.ret.transport.hireSlip.dto.HireSlipUtil;
import com.ret.transport.hireSlip.dto.LRFormRequestDTO;
import com.ret.transport.hireSlip.dto.LRFormResponseDTO;
import com.ret.transport.hireSlip.entity.LRForm;
import com.ret.transport.hireSlip.repository.LRFormRepository;
import com.ret.transport.hireSlip.service.LRFormService;

@Service("lrFormService")
@Transactional(readOnly = false)
public class LRFormServiceImpl implements LRFormService {

    private final LRFormRepository lRFormRepository;

    @Autowired
    public LRFormServiceImpl(HttpServletRequest request, HttpServletResponse response,
            LRFormRepository lRFormRepository) {
        this.lRFormRepository = lRFormRepository;
    }

    @Override
    @Transactional
    public LRForm saveOrUpdateLRForm(LRFormRequestDTO dto, Long modifiedBy) {
        LRForm lrForm = null;
        if (Validation.isNotNull(dto.getId())) {
            lrForm = findLRForm(dto.getId());
            lrForm.setUpdateInfo(modifiedBy);
        } else {
            lrForm = new LRForm();
            lrForm.setCreateInfo(modifiedBy);
        }
        validateLrForm(dto);

        lrForm.setLrNumber(dto.getLrNumber());

        /* Consignor Customer Branch */
        lrForm.setConsignor(dto.getConsignor());

        /* Consignee Customer Branch */
        lrForm.setConsignee(dto.getConsignee());

        lrForm.setStatisticalCharges(dto.getStatisticalCharges());
        lrForm.setCgst(dto.getCgst());
        lrForm.setSgst(dto.getSgst());
        lrForm.setIgst(dto.getIgst());
        lrForm.setTotal(dto.getTotal());
        lrForm.setValueRs(dto.getValueRs());
        lrForm.setActualKgs(dto.getActualKgs());
        lrForm.setChargedKgs(dto.getChargedKgs());
        lrForm.setFreightToPay(dto.getFreightToPay());
        lrForm.setUnloadingCharges(dto.getUnloadingCharges());
        lrForm.setRatePerBox(dto.getRatePerBox());
        lrForm.setDate(DateConversionUtil.convertStringToDate(dto.getDate(), ConstDateFormat.jsonDateFormat));
        return lRFormRepository.save(lrForm);
    }

    private void validateLrForm(LRFormRequestDTO dto) {
        if (Validation.isNull(dto.getConsignee())) {
            ExceptionUtil.throwException(EExceptionType.DATA_NOT_FOUND, CustomerInfoMessageUtil.CUSTOMER_NOT_FOUND);
        }
        if (Validation.isNull(dto.getConsignor())) {
            ExceptionUtil.throwException(EExceptionType.DATA_NOT_FOUND, CustomerInfoMessageUtil.CUSTOMER_NOT_FOUND);
        }
        if (Validation.isNull(dto.getTotal())) {
            ExceptionUtil.throwException(EExceptionType.DATA_NOT_FOUND, HireSlipUtil.TOTAL_AMT_NOT_FOUND);
        }
        if (Validation.isNull(dto.getValueRs())) {
            ExceptionUtil.throwException(EExceptionType.DATA_NOT_FOUND, HireSlipUtil.VALUE_NOT_FOUND);
        }
        if (Validation.isNull(dto.getDate()) || dto.getDate().equals("Invalid Date")) {
            ExceptionUtil.throwException(EExceptionType.DATA_NOT_FOUND, HireSlipUtil.DATE_NOT_FOUND);
        }
        if (Validation.isEmpty(dto.getLrNumber())) {
            ExceptionUtil.throwException(EExceptionType.DATA_NOT_FOUND, HireSlipUtil.LR_NUMBER_NOT_FOUND);
        }
    }

    @Override
    public LRForm findLRForm(Long id) {
        LRForm lRForm = getLRForm(id);
        if (Validation.isNull(lRForm)) {
            ExceptionUtil.throwException(EExceptionType.DATA_NOT_FOUND, HireSlipUtil.LR_FORM_NOT_FOUND);
        }
        return lRForm;
    }

    @Override
    public LRForm getLRForm(Long id) {
        return lRFormRepository.findByIdAndStatusTrue(id);
    }

    @Override
    @Transactional
    public void getDeleteLRForm(Long id, Long modifiedBy) {
        LRForm lRForm = findLRForm(id);
        if (Validation.isNotNull(lRForm)) {
            lRForm.setStatus(false);
            lRForm.setUpdateInfo(modifiedBy);
            lRFormRepository.save(lRForm);
        }
    }

    @Override
    @Transactional
    public LRForm saveLrForm(LRForm lrForm) {
        return lRFormRepository.save(lrForm);
    }

    @Override
    public Page<LRForm> getLRFormList(LRFormRequestDTO requestDTO) {
        SearchSpecification<LRForm> activSpec = new SearchSpecification<LRForm>(
                new SearchCriteria("status", ESearchOperator.EQUAL, true));
        Specifications<LRForm> spec = Specifications.where(activSpec);

        spec = SearchSpecificationUtil.getSpecification(spec,
                new SearchCriteria("lrNumber", ESearchOperator.LIKE, requestDTO.getLrNumber()), ECondition.AND);

        return lRFormRepository.findAll(spec, SearchSpecificationUtil.getPageableSort(requestDTO));
    }

}
