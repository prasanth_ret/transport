package com.ret.transport.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.ret.transport.user.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    User findByEmailAndStatusTrue(String email);

    User findByIdAndStatusTrue(Long id);

    User findByPhoneNumberAndStatusTrue(String phoneNumber);

}
