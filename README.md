Description : 
	 This Application focuses on a maintaining a Logistics firm which manages their Trucks, Shipments and consignments, payment information and reports.

The  component attached has a user login authenticated with JWT  and LR form which has some basic curd operations as an initial step to capture info about trucks and their managers.
	 
This is a maven component designed by using a spring boot and hibernate connecting with MS SQL Db.
	
Steps to Excecute Application:
1. Create Database with the name of "transport_base"
2. Point out the DB name at src/main/resource/datasource.properties with valid user name and password
3. If Required Change Port Number or URL Path, u can make it from src/main/resource/application.yml
4. To Build Project, execute maven goal as "clean spring-boot:run"
5. Once server started, Go to our swagger at "http://localhost:8080/transport/swagger-ui.html#/" to find the specifications