package com.ret.transport.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;
import com.ret.transport.base.util.Validation;
import com.ret.transport.security.model.JwtAuthenticationToken;

/**
 * @author Vishnu on 30-Jan-2018
 */
public class JWTAuthenticationTokenFilter extends AbstractAuthenticationProcessingFilter {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Value("${jwt.prefix}")
    private String tokenPreFix;

    public JWTAuthenticationTokenFilter() {
        super("/rest/api/**");
    }

    /**
     * Attempt to authenticate request - basically just pass over to another method to authenticate
     * request headers
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String header = request.getHeader(this.tokenHeader);
        if (Validation.isNull(header) || !header.startsWith(tokenPreFix)) {
            ExceptionUtil.throwException(EExceptionType.JWT_TOKEN_EXPIRED, "No JWT token found in request headers");
        }
        String authToken = header.substring(7);
        JwtAuthenticationToken authRequest = new JwtAuthenticationToken(authToken);
        return getAuthenticationManager().authenticate(authRequest);
    }

    /**
     * Make sure the rest of the filterchain is satisfied
     *
     * @param request
     * @param response
     * @param chain
     * @param authResult
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
            Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        // As this authentication is in HTTP header, after success we need to continue the request
        // normally
        // and return the response as if the resource was not secured at all
        chain.doFilter(request, response);
    }
}