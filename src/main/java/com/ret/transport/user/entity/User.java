package com.ret.transport.user.entity;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ret.transport.base.entity.BaseEntity;

@Entity
@Table(name = "tb_user", schema = "dbcommon")
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "pan_no")
    private String panNumber;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "alternate_number1")
    private String alternateNumber1;

    @Column(name = "alternate_number2")
    private String alternateNumber2;

    @Column(name = "license_number")
    private String licenseNumber;

    @Column(name = "dob")
    private Date dob;

    @Column(name = "is_login_available")
    private Boolean isLoginAvailable;

    @ManyToOne
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.EAGER)
    private Set<UserRoleMap> userRoleMaps = new LinkedHashSet<UserRoleMap>(0);

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public Set<UserRoleMap> getUserRoleMaps() {
        return userRoleMaps;
    }

    public void setUserRoleMaps(Set<UserRoleMap> userRoleMaps) {
        this.userRoleMaps = userRoleMaps;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAlternateNumber1() {
        return alternateNumber1;
    }

    public void setAlternateNumber1(String alternateNumber1) {
        this.alternateNumber1 = alternateNumber1;
    }

    public String getAlternateNumber2() {
        return alternateNumber2;
    }

    public void setAlternateNumber2(String alternateNumber2) {
        this.alternateNumber2 = alternateNumber2;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Boolean getIsLoginAvailable() {
        return isLoginAvailable;
    }

    public void setIsLoginAvailable(Boolean isLoginAvailable) {
        this.isLoginAvailable = isLoginAvailable;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
