/**
 * 
 */
package com.ret.transport.base.util;

/**
 * @author Vishnu on 03-Oct-2017
 */
public interface BaseCommonMessageUtil {

    String DATA_NOT_FOUND = "Data Not Found";

    String INVALID_DATA = "invalid data";

    String BOTH_ID_FOUND = "YOU CAN'T GIVE BOTH DATA AT A TIME";

    String FUTURE_DATE_NOT_ALLOWED = "Future Date Not Allowed";

    // Module
    String MODULE_NAME_NOT_FOUND = "Module Not Found : %s"; // Module

    String MODULE_NOT_FOUND = "Module Not Found";

    String INVALID_MODULE = "Invalid Module";

    // App property
    String INVALID_PROPERTY_NAME = "Invalid Property Name";

    String INVALID_PROPERTY_KEY = "Invalid Property Key";

    String APP_PROPERTY_NOT_FOUND = "Application Property Not Found %s"; // key

    String APP_PROPERTY_CATEGORY_NOT_FOUND = "App Property Category Not Found";

    // Master Data
    String MASTERDATACATEGORY_NOT_FOUND = "MasterDataCategory Not Found";

    String INVALID_MASTER_DATA_CATEGORY = "Invalid Master Data Category";

    String MASTERDATA_SHOULD_NOT_BE_NULL = "MasterData Should Not Be Null";

    String INVALID_MASTER_DATA = "Invalid Master Data";

    String MASTER_DATA_NOT_FOUND_CATEGORY = "%s Not Found for %s";

    String MASTER_DATA_NOT_FOUND = "Master Data Not Found";

    String PREVIOUS_DATE_COULD_NOT_BE_ALLOWED = "Previous Date Could Not Be Allowed";

    // // user
    // String USER_NOT_FOUND_ANY_ROLE = "user not found any role";
    //
    // String USER_NOT_FOUND_ANY_PHACE = "User not found any Phase";
    //
    // String USER_DATA_NOT_NULL = "user data not found";
    //
    // String USER_PHASE_NULL = "user phase cannot be null or empty";
    //
    // String USER_ROLE_NULL = "user Roles cannot be null";
    //
    // String IS_STAFF_TYPE_NULL = "isStaffType cannot be null";
    //
    // String CHECK_LOGIN = "pls login first! ";
    //
    // String USER_NOT_EXIST = "user not exists or null";
    //
    // String DINING_DATA_NOT_EXIST = "dining data NOT FOUND or Exists";
    //
    // String ADDRESS_NOT_EXIST = "address not found /exists";
    //
    // String ENUM_NULL = "enum cannot be null";
    //
    // String EMAIL_EXIST = "EMAIL already exits ";
    //
    // String EMAIL_NOT_VALID = "email not valid";
    //
    // String USERNAME_EXIST = "user name already exits ";
    //
    // String ROLE_NULL = "role should not be null or empty";
    //
    // String PHASE_NULL = "phase cannot be null";
    //
    // String PHASE_UNABLE_TO_SAVE = "Phase unable To Save";
    //
    // String PHASEHOUSE_NULL = "phasehouse cannot be null";
    //
    // String USERPHASE_NULL = "userphase cannot be null";
    //
    // String TENANT_NOT_THERE = "there is no tenant in the house number";
    //
    // String CHECK_EMAIL_OR_PASSWORD = "check EMAIL or password are entered
    // wrong!";
    //
    // String EMAIL_OR_PASS_NOT_NULL = "email or password cannot be null";
    //
    // String PASSWORD_NOT_MATCH = "password not match";
    //
    // String CHECK_USERNAME_AND_PASSWORD = "check userName and password are
    // entered wrong!";
    //
    // String DINING_UNABLE_TO_SAVE = "Dining unable To Save";
    //
    // String PHASEHOUSE_UNABLE_TO_SAVE = "PhaseHouse unable To Save";
    //
    // String DININGDETAILS_UNABLE_TO_SAVE = "DiningDetails unable To Save";
    //
    // String DININGDETAILS_NULL = "DiningDetails cannot be null";
    //
    // String PHASEHOUSE_DETAILS_NOT_FOUND_FOR_GIVEN_ID = "PhaseHouse Details
    // Not Found for Given
    // Id";
    //
    // String DININGDETAILS_NOT_FOUND_FOR_THE_GIVEN_ID = "DeiningDetails Not
    // Found For The Given
    // Id";
    //
    // String ADDRESS_UNABLE_TO_SAVE = "Address Unable To Save";
    //
    // String PHASE_DETAILS_NOT_FOUND_FOR_GIVEN_ID = "Phase Details Not Found
    // for Given Id ";
    //
    // String PHASE_ID_NOT_FOUND = "Phase Id Not Found";

}