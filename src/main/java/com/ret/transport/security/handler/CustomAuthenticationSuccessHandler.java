package com.ret.transport.security.handler;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ret.transport.security.transfer.JWTUserDTO;
import com.ret.transport.security.util.JWTTokenGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

/**
 * @author Vishnu on 30-Jan-2018
 */
@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
            Authentication authentication) throws IOException, ServletException {
        JWTUserDTO jwtUserDto = JWTUserDTO.buildFromAuthentication(authentication);
        httpServletResponse.setStatus(HttpServletResponse.SC_CREATED);
        httpServletResponse.getWriter().append(JWTTokenGenerator.generateToken(jwtUserDto, jwtSecret)).flush();
    }
}
