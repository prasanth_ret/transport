package com.ret.transport.base.io;

import java.util.List;

import com.ret.transport.base.enums.EResponseStatusCode;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class ListResponse<T> extends BaseResponse {
    private static final long serialVersionUID = -3584688466621772037L;

    private int count;
    private List<T> results;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
        if (results != null) {
            this.count = results.size();
        }
    }

    public void setSuccess(List<T> results) {
        setStatus(EResponseStatusCode.SUCCESS);
        setResults(results);
    }

    public void setSuccess() {
        setStatus(EResponseStatusCode.SUCCESS);
    }

}
