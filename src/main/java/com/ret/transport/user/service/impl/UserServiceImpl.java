
package com.ret.transport.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;
import com.ret.transport.base.util.Validation;
import com.ret.transport.security.model.AuthenticatedUser;
import com.ret.transport.user.entity.User;
import com.ret.transport.user.repository.UserRepository;
import com.ret.transport.user.service.UserService;

@Service("userService")
@Transactional(readOnly = false)
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User checkAuthUser(Authentication authentication) {
        AuthenticatedUser authUser = (AuthenticatedUser) authentication.getPrincipal();
        return userRepository.findByIdAndStatusTrue(authUser.getId());
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findByEmailAndStatusTrue(email);
    }

    @Override
    public User getUserByPhoneNumber(String phoneNumber) {
        return userRepository.findByPhoneNumberAndStatusTrue(phoneNumber);
    }

    @Override
    public User findUserByEmail(String email) {
        User user = getUserByEmail(email);
        if (Validation.isNull(user)) {
            ExceptionUtil.throwException(EExceptionType.DATA_NOT_FOUND,
                    String.format("User not found for given UserName : %s", email));
        }
        return user;
    }

    @Override
    public User findUserByPhoneNumber(String phoneNumber) {
        User user = getUserByPhoneNumber(phoneNumber);
        if (Validation.isNull(user)) {
            ExceptionUtil.throwException(EExceptionType.DATA_NOT_FOUND,
                    String.format("User not found for given UserName : %s", phoneNumber));
        }
        return user;
    }

}
