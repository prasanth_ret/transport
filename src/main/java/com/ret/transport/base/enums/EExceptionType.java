package com.ret.transport.base.enums;

public enum EExceptionType {

    DATA_NOT_FOUND,
    DATA_ALREADY_EXISTS,
    INVALID_DATA,
    DOCUMENT_NOT_FOUND,
    UNAUTHORIZED,
    JWT_TOKEN_EXPIRED,
    JWT_TOKEN_MALFORMED,
    JWT_TOKEN_MISSING,
    BAD_REQUEST;

}
