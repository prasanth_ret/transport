package com.ret.transport.hireSlip.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.enums.EResponseStatusCode;
import com.ret.transport.base.exception.ExceptionUtil;
import com.ret.transport.base.io.Response;
import com.ret.transport.base.search.PageResponseDTO;
import com.ret.transport.base.util.ConstDateFormat;
import com.ret.transport.base.util.DateConversionUtil;
import com.ret.transport.base.util.Validation;
import com.ret.transport.hireSlip.dto.LRFormConversionUtil;
import com.ret.transport.hireSlip.dto.LRFormRequestDTO;
import com.ret.transport.hireSlip.dto.LRFormResponseDTO;
import com.ret.transport.hireSlip.entity.LRForm;
import com.ret.transport.hireSlip.service.LRFormService;
import com.ret.transport.user.entity.User;
import com.ret.transport.user.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "TrasnportApp", description = "LRForm Services")
@RequestMapping("/rest/api")
public class LRFormController {

    private final UserService userService;
    private final LRFormService lrFormService;

    @Autowired
    public LRFormController(UserService userService, LRFormService lrFormService) {
        this.userService = userService;
        this.lrFormService = lrFormService;
    }

    /* LRFORM */

    @ApiOperation(value = "Create LRForm", notes = "SuperAdmin/Admin can create LRForm")
    @PreAuthorize("hasAuthority('Super Admin') or hasAuthority('Admin')")
    @PostMapping("/lr")
    public Response<String> createLRForm(@RequestBody @Valid @NotBlank LRFormRequestDTO requestDTO,
            Authentication authentication) {
        User authUser = userService.checkAuthUser(authentication);
        Response<String> response = new Response<>();
        requestDTO.setLoginId(authUser.getId());
        lrFormService.saveOrUpdateLRForm(requestDTO, requestDTO.getLoginId());
        response.setStatus(EResponseStatusCode.SUCCESS);
        return response;
    }

    @ApiOperation(value = "Update LRForm", notes = "SuperAdmin/Admin can update LRForm")
    @PreAuthorize("hasAuthority('Super Admin') or hasAuthority('Admin')")
    @CrossOrigin("*")
    @PutMapping("/lr")
    public Response<String> updateLRform(@RequestBody @Valid @NotBlank LRFormRequestDTO requestDTO,
            Authentication authentication) {
        User authUser = userService.checkAuthUser(authentication);
        Response<String> response = new Response<>();
        requestDTO.setLoginId(authUser.getId());
        lrFormService.saveOrUpdateLRForm(requestDTO, requestDTO.getLoginId());
        response.setStatus(EResponseStatusCode.SUCCESS);
        return response;

    }

    /* View LRForm */
    @ApiOperation(value = "View  Single LRForm Detail", notes = "SuperAdmin/Admin can View LRform Detail")
    @PreAuthorize("hasAuthority('Super Admin') or hasAuthority('Admin')")
    @GetMapping("/lr/{id}")
    public Response<LRFormRequestDTO> viewSingleLRForm(@PathVariable @Valid @NotNull(message = "Invalid ID") Long id) {
        Response<LRFormRequestDTO> response = new Response<>();
        LRForm lrForm = lrFormService.findLRForm(id);
        response.setStatus(EResponseStatusCode.SUCCESS);
        response.setResult(LRFormConversionUtil.convertEntityToDTO(lrForm));
        response.setStatus(EResponseStatusCode.SUCCESS);
        return response;
    }

    /* View LRForm */
    @ApiOperation(value = "View LRForm List Pagiantion", notes = "SuperAdmin/Admin can View LRform List")
    // @PreAuthorize("hasAuthority('Super Admin') or hasAuthority('Admin')")
    @PostMapping("/lrs")
    public Response<PageResponseDTO> viewLRFormList(@RequestBody @Valid @NotBlank LRFormRequestDTO requestDTO,
            Authentication authentication) {
        User authUser = userService.checkAuthUser(authentication);
        requestDTO.setLoginId(authUser.getId());
        Page<LRForm> customerPage = lrFormService.getLRFormList(requestDTO);
        List<LRForm> userlist = customerPage.getContent();
        if (Validation.isEmpty(userlist)) {
            ExceptionUtil.throwException(EExceptionType.DATA_NOT_FOUND, "Data Not Found");
        }
        Response<PageResponseDTO> response = new Response<>();
        PageResponseDTO dto = new PageResponseDTO();
        List<LRFormRequestDTO> dtos = userlist.stream().map(this::convertEntityToDTOss).collect(Collectors.toList());
        dto.setTotalData((int) customerPage.getTotalElements());
        dto.setTotalPages((int) customerPage.getTotalPages());
        dto.setDataLength(requestDTO.getDataLength());
        dto.setPageIndex(requestDTO.getPageIndex());
        dto.setSortDirection(requestDTO.getSortDirection());
        dto.setSortingColumn(requestDTO.getSortingColumn());
        dto.setResponseData(dtos);
        response.setStatus(EResponseStatusCode.SUCCESS);
        response.setResult(dto);
        return response;
    }

    private LRFormRequestDTO convertEntityToDTOss(LRForm entity) {
        LRFormRequestDTO dto = new LRFormRequestDTO();
        dto.setId(entity.getId());
        dto.setLrNumber(entity.getLrNumber());
        dto.setConsignee(entity.getConsignee());
        dto.setConsignor(entity.getConsignor());
        dto.setStatisticalCharges(entity.getStatisticalCharges());
        dto.setCgst(entity.getCgst());
        dto.setSgst(entity.getSgst());
        dto.setIgst(entity.getIgst());
        dto.setTotal(entity.getTotal());
        dto.setValueRs(entity.getValueRs());
        dto.setChargedKgs(entity.getChargedKgs());
        dto.setFreightToPay(entity.getFreightToPay());
        dto.setUnloadingCharges(entity.getUnloadingCharges());
        dto.setRatePerBox(entity.getRatePerBox());
        dto.setDate(DateConversionUtil.convertDate(entity.getDate(), ConstDateFormat.jsonDateFormat));
        dto.setActualKgs(entity.getActualKgs());
        return dto;
    }

    /* Soft Deletion */
    @ApiOperation(value = "LRForm Delete", notes = "SuperAdmin/Admin can delete LRForm")
    @PreAuthorize("hasAuthority('Super Admin') or hasAuthority('Admin')")
    @GetMapping("/delete/{id}")
    public Response<String> deleteLRform(@Valid @NotNull @PathVariable Long id, Authentication authentication) {
        Response<String> response = new Response<>();
        User authUser = userService.checkAuthUser(authentication);
        lrFormService.getDeleteLRForm(id, authUser.getId());
        response.setStatus(EResponseStatusCode.SUCCESS);
        return response;
    }

}
