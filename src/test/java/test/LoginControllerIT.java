package test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.json.JSONObject;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.ret.transport.login.dto.LoginRequestDTO;

import base.BaseIT;
import base.BaseUtil;

public class LoginControllerIT extends BaseIT {

    private final String LOGIN_URL = "/api/user/login";

    @Test
    public void userValidLogin() throws Exception {
        LoginRequestDTO loginDTO = new LoginRequestDTO();
        loginDTO.setUserName("9874563210");
        loginDTO.setPassword("123456");
        mockMvc.perform(
                post(LOGIN_URL).contentType(MediaType.APPLICATION_JSON).content(BaseUtil.convertObjToJson(loginDTO)))
                .andExpect(status().isOk()).andExpect(jsonPath("$.status").value("SUCCESS"));
    }

}
