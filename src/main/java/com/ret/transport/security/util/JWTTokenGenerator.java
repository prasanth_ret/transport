package com.ret.transport.security.util;

import java.util.Date;

import org.joda.time.DateTime;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.enums.ETimeDiffers;
import com.ret.transport.base.exception.ExceptionUtil;
import com.ret.transport.base.util.Validation;
import com.ret.transport.security.transfer.JWTUserDTO;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author Vishnu on 30-Jan-2018
 */
public class JWTTokenGenerator {

    public static String CLAIM_ROLE_KEY = "role";
    public static String CLAIM_USER_ID_KEY = "userId";
    public static String JWT_EXPIRY_TYPE = ETimeDiffers.HOUR.getValue();
    public static int SESSION_TIME_OUT = 24;

    public static String generateToken(JWTUserDTO jwtUserDTO, String secret) {
        Date expiryDate = getTokenExpiryDate();
        if (Validation.isNull(expiryDate)) {
            ExceptionUtil.throwException(EExceptionType.INVALID_DATA, "Invalid JWT Expiry Date");
        }
        Claims claims = Jwts.claims().setSubject(jwtUserDTO.getUserName());
        claims.put(CLAIM_ROLE_KEY, jwtUserDTO.getRole());
        claims.put(CLAIM_USER_ID_KEY, jwtUserDTO.getId());
        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, secret).setExpiration(expiryDate)
                .compact();
    }

    private static Date getTokenExpiryDate() {
        ETimeDiffers eTime = ETimeDiffers.getEnum(JWT_EXPIRY_TYPE);
        switch (eTime) {
        case YEAR:
            return DateTime.now().plusYears(SESSION_TIME_OUT).toDate();
        case MONTH:
            return DateTime.now().plusMonths(SESSION_TIME_OUT).toDate();
        case WEEK:
            return DateTime.now().plusWeeks(SESSION_TIME_OUT).toDate();
        case DAY:
            return DateTime.now().plusDays(SESSION_TIME_OUT).toDate();
        case HOUR:
            return DateTime.now().plusHours(SESSION_TIME_OUT).toDate();
        case MINUTE:
            return DateTime.now().plusMinutes(SESSION_TIME_OUT).toDate();
        case SECOND:
            return DateTime.now().plusSeconds(SESSION_TIME_OUT).toDate();
        case MILLI_SECOUND:
            return DateTime.now().plusMillis(SESSION_TIME_OUT).toDate();
        }
        return null;
    }
}
