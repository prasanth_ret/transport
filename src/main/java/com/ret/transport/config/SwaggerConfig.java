package com.ret.transport.config;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;

import com.fasterxml.classmate.TypeResolver;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Vishnu on 08-Feb-2018
 */
@Configuration
@EnableSwagger2
@ComponentScan(basePackages = "com.ret")
public class SwaggerConfig {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private TypeResolver typeResolver;

    @Bean
    public Docket customImplementation() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage(WebAppUtil.BASE_PACKAGE)).paths(PathSelectors.any()).build()
                .enable(true).apiInfo(getApiInfo()).directModelSubstitute(LocalDate.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(new AlternateTypeRule(
                        typeResolver.resolve(DeferredResult.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                        typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(false).securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext())).enableUrlTemplating(true);
    }

    private ApiKey apiKey() {
        return new ApiKey(this.tokenHeader, this.tokenHeader, ApiKeyVehicle.HEADER.getValue());
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/rest/api.*"))
                .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference(this.tokenHeader, authorizationScopes));
    }

    @Bean
    public SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder().clientId("trasnport-app-client-id")
                .clientSecret("trasnport-app-client-secret").realm("trasnport-app-realm").appName("trasnport-app")
                .scopeSeparator(",").additionalQueryStringParams(null).useBasicAuthenticationWithAccessCodeGrant(false)
                .build();
    }

    @Bean
    public UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder().deepLinking(true).displayOperationId(false).defaultModelsExpandDepth(1)
                .defaultModelExpandDepth(1).defaultModelRendering(ModelRendering.EXAMPLE).displayRequestDuration(false)
                .docExpansion(DocExpansion.NONE).filter(false).maxDisplayedTags(null)
                .operationsSorter(OperationsSorter.ALPHA).showExtensions(false).tagsSorter(TagsSorter.ALPHA)
                .validatorUrl(null).build();
    }

    @Value("${api.title}")
    private String apiTitle;

    @Value("${api.description}")
    private String apiDescription;

    @Value("${api.version}")
    private String apiVersion;

    @Value("${api.termsOfServiceUrl}")
    private String apiTermsOfServiceUrl;

    @Value("${api.contact.name}")
    private String apiContactName;

    @Value("${api.contact.url}")
    private String apiContactURL;

    @Value("${api.contact.email}")
    private String apiContactEmail;

    @Value("${api.license}")
    private String apiLicense;

    @Value("${api.licenseUrl}")
    private String apiLicenseURL;

    private ApiInfo getApiInfo() {
        return new ApiInfo(apiTitle, apiDescription, apiVersion, apiTermsOfServiceUrl, getAPIContact(), apiLicense,
                apiLicenseURL, new ArrayList<>(0));
    }

    private Contact getAPIContact() {
        return new Contact(apiContactName, apiContactURL, apiContactEmail);
    }

}
