package com.ret.transport.security.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;
import com.ret.transport.security.transfer.JWTUserDTO;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;

/**
 * @author Vishnu on 30-Jan-2018
 */
@Component
public class JWTTokenValidator {

    @Value("${jwt.secret}")
    private String secret;

    /**
     * Tries to parse specified String as a JWT token. If successful, returns User object with
     * username, id and role prefilled (extracted from token). If unsuccessful (token is invalid or
     * not containing all required user properties), simply returns null.
     *
     * @param token
     *            the JWT token to parse
     * @return the User object extracted from specified token or null if a token is invalid.
     */
    public JWTUserDTO parseToken(String token) {
        JWTUserDTO jwtUserDTO = null;
        try {
            Claims body = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
            jwtUserDTO = new JWTUserDTO();
            jwtUserDTO.setUserName(body.getSubject());
            jwtUserDTO.setRole((String) body.get(JWTTokenGenerator.CLAIM_ROLE_KEY));
            Integer id = (Integer) body.get(JWTTokenGenerator.CLAIM_USER_ID_KEY);
            jwtUserDTO.setId(Long.valueOf(id));
        } catch (ExpiredJwtException e) {
            ExceptionUtil.throwException(EExceptionType.JWT_TOKEN_EXPIRED, e.getMessage());
        }
        return jwtUserDTO;
    }
}
