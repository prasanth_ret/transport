--
-- DBADJUDICATOR SCHEMA DATA
-- 

--
-- Modules Data --
--

INSERT INTO dbcommon.tb_address (id, address, city, pin_code, country_code, country, phone_number, mobile_number, fax_number, status, created_by, created_at, modified_by, modified_at) 
VALUES (1, N'Sitra', NULL, N'641014', N'225', N'India', N'9876543210', N'25221556', N'6555', 1, 0, CURRENT_TIMESTAMP(), 0, CURRENT_TIMESTAMP());


INSERT INTO dbcommon.tb_user (id, first_name, last_name, email, pan_no, phone_number, password, dob, license_number, address_id, is_login_available, status, created_by, created_at, modified_by, modified_at) 
VALUES (1, N'Super Admin', NULL, N'superadmin@example.com', N'BQPPS0878', N'9874563210', N'$2a$10$Fu6H7wTcFQRtB4.YIFsbwuevoFczMl2LjVe.RGNFu7oqIJATu1iOm', CURRENT_TIMESTAMP(), N'1457320R', 1, 1, 1, 0, CURRENT_TIMESTAMP(), 0, CURRENT_TIMESTAMP());

INSERT INTO dbcommon.tb_role (id, name, description, status, created_by, created_at, modified_by, modified_at)
values (1, N'Super Admin', N'Super Admin', 1, 0, CURRENT_TIMESTAMP(), 0, CURRENT_TIMESTAMP());

INSERT INTO dbcommon.tb_user_role_map (id, user_id, role_id, account_status , status, created_by, created_at, modified_by, modified_at)
values (1, 1, 1, N'Active', 1, 0, CURRENT_TIMESTAMP(), 0, CURRENT_TIMESTAMP());

INSERT INTO dbcommon.tb_lr_form (id, lr_number, consignor, consignee, rate_per_box, statistical_charges, cgst, sgst, igst, total, value_rs, charged_kgs, freight_to_pay, unloading_charges, date, bill_no, bill_date, actual_kgs, status, created_by, created_at, modified_by, modified_at) 
VALUES (1, N'LR00001', N'Aqua, Coimbatore', N'Aqua, Indore', CAST(1.50 AS Decimal(15, 2)), CAST(15.00 AS Decimal(15, 2)), CAST(0.00 AS Decimal(15, 2)), CAST(0.00 AS Decimal(15, 2)), CAST(0.00 AS Decimal(15, 2)), CAST(10000.00 AS Decimal(15, 2)), CAST(125012.00 AS Decimal(15, 2)), CAST(5000.00 AS Decimal(15, 2)), CAST(10000.00 AS Decimal(15, 2)), CAST(0.00 AS Decimal(15, 2)), CURRENT_TIMESTAMP(), NULL, NULL, CAST(5000.00 AS Decimal(15, 2)), 1, 0, CURRENT_TIMESTAMP(), 0, CURRENT_TIMESTAMP());
