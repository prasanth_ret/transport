package com.ret.transport.hireSlip.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ret.transport.base.entity.BaseEntity;

@Entity
@Table(name = "tb_lr_form", schema = "dbcommon")
public class LRForm extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = "lr_number")
    private String lrNumber;

    @Column(name = "consignor")
    private String consignor;

    @Column(name = "consignee")
    private String consignee;

    @Column(name = "rate_per_box")
    private Double ratePerBox;

    @Column(name = "statistical_charges")
    private Double statisticalCharges;

    @Column(name = "cgst")
    private Double cgst;

    @Column(name = "sgst")
    private Double sgst;

    @Column(name = "igst")
    private Double igst;

    @Column(name = "total")
    private Double total;

    @Column(name = "value_rs")
    private Double valueRs;

    @Column(name = "charged_kgs")
    private Double chargedKgs;

    @Column(name = "freight_to_pay")
    private Double freightToPay;

    @Column(name = "unloading_charges")
    private Double unloadingCharges;

    @Column(name = "date")
    private Date date;

    @Column(name = "bill_no")
    private String billNumber;

    @Column(name = "bill_date")
    private Date billDate;

    @Column(name = "actual_kgs")
    private Double actualKgs;

    public String getLrNumber() {
        return lrNumber;
    }

    public void setLrNumber(String lrNumber) {
        this.lrNumber = lrNumber;
    }

    public String getConsignor() {
        return consignor;
    }

    public void setConsignor(String consignor) {
        this.consignor = consignor;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public Double getStatisticalCharges() {
        return statisticalCharges;
    }

    public void setStatisticalCharges(Double statisticalCharges) {
        this.statisticalCharges = statisticalCharges;
    }

    public Double getCgst() {
        return cgst;
    }

    public void setCgst(Double cgst) {
        this.cgst = cgst;
    }

    public Double getSgst() {
        return sgst;
    }

    public void setSgst(Double sgst) {
        this.sgst = sgst;
    }

    public Double getIgst() {
        return igst;
    }

    public void setIgst(Double igst) {
        this.igst = igst;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getValueRs() {
        return valueRs;
    }

    public void setValueRs(Double valueRs) {
        this.valueRs = valueRs;
    }

    public Double getChargedKgs() {
        return chargedKgs;
    }

    public void setChargedKgs(Double chargedKgs) {
        this.chargedKgs = chargedKgs;
    }

    public Double getFreightToPay() {
        return freightToPay;
    }

    public void setFreightToPay(Double freightToPay) {
        this.freightToPay = freightToPay;
    }

    public Double getUnloadingCharges() {
        return unloadingCharges;
    }

    public void setUnloadingCharges(Double unloadingCharges) {
        this.unloadingCharges = unloadingCharges;
    }

    public Double getRatePerBox() {
        return ratePerBox;
    }

    public void setRatePerBox(Double ratePerBox) {
        this.ratePerBox = ratePerBox;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public Double getActualKgs() {
        return actualKgs;
    }

    public void setActualKgs(Double actualKgs) {
        this.actualKgs = actualKgs;
    }

}
