package com.ret.transport.base.exception.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class InvalidDataException extends RuntimeException {
    private static final long serialVersionUID = 7710147794055702995L;

    private static final Log LOG = LogFactory.getLog(InvalidDataException.class);

    public InvalidDataException() {
        super();
    }

    /**
     * @param message
     * @param cause
     */
    public InvalidDataException(String message, Throwable cause) {
        super(message, cause);
        LOG.error(message);
    }

    /**
     * @param message
     */
    public InvalidDataException(String message) {
        super(message);
        LOG.error(message);
    }

    /**
     * @param cause
     */
    public InvalidDataException(Throwable cause) {
        super(cause);
    }

}
