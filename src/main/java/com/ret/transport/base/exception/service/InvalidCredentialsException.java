package com.ret.transport.base.exception.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class InvalidCredentialsException extends RuntimeException {
    private static final long serialVersionUID = -3398470957366992134L;

    private static final Log LOG = LogFactory.getLog(InvalidCredentialsException.class);

    /**
     * @param message
     * @param cause
     */
    public InvalidCredentialsException(String message, Throwable cause) {
        super(message, cause);
        LOG.error(message);
    }

    /**
     * @param message
     */
    public InvalidCredentialsException(String message) {
        super(message);
        LOG.error(message);
    }

    /**
     * @param cause
     */
    public InvalidCredentialsException(Throwable cause) {
        super(cause);
    }

}
