package com.ret.transport.base.search.enums;

/**
 * @author Vishnu on 01-Oct-2017
 */
public enum ECondition {
    AND("And"), OR("Or");

    private String value;

    ECondition(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
