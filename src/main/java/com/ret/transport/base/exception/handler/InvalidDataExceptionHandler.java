package com.ret.transport.base.exception.handler;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ret.transport.base.enums.EResponseStatusCode;
import com.ret.transport.base.exception.service.InvalidDataException;
import com.ret.transport.base.io.BaseResponse;

/**
 * @author Vishnu on 01-Oct-2017
 */
@ControllerAdvice
public class InvalidDataExceptionHandler {
    @ExceptionHandler(InvalidDataException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public BaseResponse invalidData(InvalidDataException exception) {
        BaseResponse response = new BaseResponse(EResponseStatusCode.FAILURE);
        response.setErrorCode(HttpServletResponse.SC_FORBIDDEN);
        response.setErrorMessage(exception.getMessage());
        return response;
    }
}
