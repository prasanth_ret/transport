package com.ret.transport.base.exception.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Vishnu on 08-Feb-2018
 */
public class BadRequestException extends RuntimeException {
    private static final long serialVersionUID = 3481889740944533624L;

    private static final Log LOG = LogFactory.getLog(BadRequestException.class);

    /**
     * @param message
     * @param cause
     */
    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
        LOG.error(message);
    }

    /**
     * @param message
     */
    public BadRequestException(String message) {
        super(message);
        LOG.error(message);
    }

    /**
     * @param cause
     */
    public BadRequestException(Throwable cause) {
        super(cause);

    }

}
