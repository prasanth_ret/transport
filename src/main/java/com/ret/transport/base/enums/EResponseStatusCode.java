package com.ret.transport.base.enums;

/**
 * @author Vishnu on 01-Oct-2017
 */
public enum EResponseStatusCode {

    SUCCESS, FAILURE
}
