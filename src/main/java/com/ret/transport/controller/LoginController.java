package com.ret.transport.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.enums.EResponseStatusCode;
import com.ret.transport.base.exception.ExceptionUtil;
import com.ret.transport.base.io.Response;
import com.ret.transport.base.util.Validation;
import com.ret.transport.login.dto.LoginRequestDTO;
import com.ret.transport.login.dto.LoginResponseDTO;
import com.ret.transport.login.service.LoginService;
import com.ret.transport.security.transfer.JWTUserDTO;
import com.ret.transport.security.util.JWTTokenGenerator;
import com.ret.transport.user.entity.User;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/user")
public class LoginController {

    @Value("${jwt.secret}")
    private String jwtSecret;

    private final LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @ApiOperation(value = "Login", notes = "SuperAdmin/Admin can login")
    @PostMapping("/login")
    public Response<LoginResponseDTO> login(@RequestBody @Valid @NotNull LoginRequestDTO loginModel,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            ExceptionUtil.throwException(EExceptionType.BAD_REQUEST,
                    Validation.getBadRequestErrorMessage(bindingResult.getAllErrors()));
        }
        Response<LoginResponseDTO> response = new Response<>();
        LoginResponseDTO dto = new LoginResponseDTO();
        User user = loginService.loginAuthentication(loginModel);
        JWTUserDTO jwtUserDto = JWTUserDTO.buildFromAuthentication(user);
        String token = JWTTokenGenerator.generateToken(jwtUserDto, jwtSecret);
        dto = convertUserEntityToDTO(user);
        dto.setToken(token);
        response.setStatus(EResponseStatusCode.SUCCESS);
        response.setResult(dto);
        return response;
    }

    public static LoginResponseDTO convertUserEntityToDTO(User entity) {
        LoginResponseDTO dto = new LoginResponseDTO();
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setEmail(entity.getEmail());
        dto.setPhoneNumber(entity.getPhoneNumber());
        return dto;
    }

}
