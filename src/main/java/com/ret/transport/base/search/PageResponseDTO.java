package com.ret.transport.base.search;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class PageResponseDTO extends SearchParam {

    private static final long serialVersionUID = 1L;
    private List<?> responseData = new ArrayList<>(0);

    public List<?> getResponseData() {
        return responseData;
    }

    public void setResponseData(List<?> responseData) {
        this.responseData = responseData;
    }

}
