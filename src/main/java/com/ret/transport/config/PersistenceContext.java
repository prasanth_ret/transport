package com.ret.transport.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.zaxxer.hikari.HikariDataSource;

/**
 * @author Vishnu on 01-Oct-2017
 */

@Configuration
@EnableWebMvc
@EnableJpaRepositories(basePackages = "com.ret")
@ComponentScan("com.ret")
@EnableTransactionManagement
public class PersistenceContext implements WebMvcConfigurer {

    @Configuration
    @Profile("default")
    @PropertySource("classpath:datasource.properties")
    static class Defaults {
    }

    @Configuration
    @Profile("h2")
    @PropertySource("classpath:datasource-h2.properties")
    static class H2 {
    }

    @Autowired
    Environment env;

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource());
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setJpaProperties(jpaProperties());
        entityManagerFactoryBean.setPackagesToScan(WebAppUtil.BASE_PACKAGE);
        return entityManagerFactoryBean;
    }

    @Bean
    @Qualifier(value = "jpaTransactionManager") // Updated Qualifier name for to handle Workflow's
                                                // transaction manager
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    @Bean(name = "dataSource", destroyMethod = "close")
    public DataSource dataSource() {
        return getDataSource("db.driver", "db.url", "db.username", "db.password");
    }

    @Bean
    public JpaDialect jpaDialect() {
        return new HibernateJpaDialect();
    }

    private Properties jpaProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", env.getRequiredProperty("hibernate.hbm2ddl.auto"));
        properties.setProperty("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
        properties.setProperty("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));
        properties.setProperty("hibernate.format_sql", env.getRequiredProperty("hibernate.format_sql"));
        // properties.setProperty("hibernate.default_schema",
        // env.getRequiredProperty("hibernate.default_schema"));
        return properties;
    }

    private DataSource getDataSource(String driver, String url, String user, String password) {
        final HikariDataSource ds = new HikariDataSource();
        ds.setDriverClassName(env.getRequiredProperty(driver));
        ds.setJdbcUrl(env.getRequiredProperty(url));
        ds.setUsername(env.getRequiredProperty(user));
        ds.setPassword(env.getRequiredProperty(password));
        return ds;
    }
    //
    // @Bean(name = "multipartResolver")
    // public CommonsMultipartResolver multipartResolver() {
    // CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
    // commonsMultipartResolver.setDefaultEncoding("utf-8");
    // commonsMultipartResolver.setMaxUploadSizePerFile(5242880);// 5MB
    // return commonsMultipartResolver;
    // }
}
