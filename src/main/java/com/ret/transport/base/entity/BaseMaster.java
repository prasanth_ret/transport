package com.ret.transport.base.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.springframework.transaction.annotation.Transactional;

/**
 * @author Vishnu on 01-Oct-2017
 */
@MappedSuperclass
@Transactional(value = "jpaTransactionManager")
public abstract class BaseMaster extends BaseEntity {

    private static final long serialVersionUID = -4321754545072981830L;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
