package com.ret.transport.hireSlip.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ret.transport.hireSlip.entity.LRForm;

public interface LRFormRepository extends JpaRepository<LRForm, Long>, JpaSpecificationExecutor<LRForm> {

    LRForm findByIdAndStatusTrue(Long id);

}
