package com.ret.transport.base.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.runtime.resource.loader.StringResourceLoader;
import org.apache.velocity.runtime.resource.util.StringResourceRepository;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class VelocityUtil {
    private static VelocityEngine velocityEngine;
    private static VelocityEngine velocityEngineForString;

    static {
        getVelocityEngine();
        getVelocityEngineForString();
    }

    private static VelocityEngine getVelocityEngine() {
        velocityEngine = new VelocityEngine();
        Properties properties = new Properties();
        properties.setProperty("resource.loader", "class");
        properties.setProperty("class.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init(properties);
        return velocityEngine;
    }

    private static VelocityEngine getVelocityEngineForString() {
        velocityEngineForString = new VelocityEngine();
        Properties properties = new Properties();
        properties.setProperty("resource.loader", "string");
        properties.setProperty("class.resource.loader.class", StringResourceLoader.class.getName());
        properties.setProperty("string.resource.loader.repository.static", "false");
        velocityEngineForString.init(properties);

        return velocityEngineForString;
    }

    private static StringResourceRepository getStringRepo() {
        StringResourceRepository repo = (StringResourceRepository) velocityEngineForString
                .getApplicationAttribute(StringResourceLoader.REPOSITORY_NAME_DEFAULT);
        return repo;
    }

    public static void updateTemplate(String templateName, String content) {
        getStringRepo().removeStringResource(templateName);
        getStringRepo().putStringResource(templateName, content);
    }

    public static String getClassPathFileContent(String templatePath) {
        Resource resource = null;
        Scanner contentScanner = null;
        String content = null;
        try {
            resource = new ClassPathResource(templatePath);
            contentScanner = new Scanner(resource.getFile()).useDelimiter("\\Z");
            content = contentScanner.next();
        } catch (IOException e) {
            // something went wrong here
        } finally {
            if (contentScanner != null) {
                contentScanner.close();
            }
        }
        return content;
    }

    // public static String getTemplateContent(TemplateDTO templateDTO, Map context) {
    // StringResource stringResource =
    // getStringRepo().getStringResource(templateDTO.getTemplateName());
    //
    // if (stringResource == null) {
    // getStringRepo().putStringResource(templateDTO.getTemplateName(), templateDTO.getContent());
    // }
    //
    // Template vTemplate = velocityEngineForString.getTemplate(templateDTO.getTemplateName());
    //
    // VelocityContext velocityContext = new VelocityContext(context);
    // StringWriter mailBodyWriter = new StringWriter();
    // vTemplate.merge(velocityContext, mailBodyWriter);
    //
    // return mailBodyWriter.toString();
    // }

    public static String getBody(String templatePath, Map context) {
        Template vTemplate = getTemplate(templatePath);
        VelocityContext velocityContext = new VelocityContext(context);
        StringWriter mailBodyWriter = new StringWriter();
        vTemplate.merge(velocityContext, mailBodyWriter);
        return mailBodyWriter.toString();
    }

    public static Template getTemplate(String templateFile) {
        return velocityEngine.getTemplate(templateFile);
    }

    /* To Merge SMS Content with Map values */
    public static String getSMSMessage(String smsContent, Map context) {
        StrSubstitutor substitutor = new StrSubstitutor(context);
        return substitutor.replace(smsContent);
    }
}
