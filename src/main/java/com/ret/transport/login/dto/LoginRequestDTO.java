package com.ret.transport.login.dto;

import org.hibernate.validator.constraints.NotBlank;

import com.ret.transport.base.dto.BaseDTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Vishnu
 */
@ApiModel(value = "Login Request", description = "Login Reqeust DTO")
public class LoginRequestDTO extends BaseDTO {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "UserId should be Mandatory")
    @ApiModelProperty(value = "userName", dataType = "java.lang.String", required = true)
    private String userName;

    @NotBlank(message = "Password should be Mandatory")
    @ApiModelProperty(value = "password", dataType = "java.lang.String", required = true)
    private String password;

    private String dob;
    private String newPassword;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

}
