package com.ret.transport.base.exception.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class DocumentNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -4698296532677066373L;

    private static final Log LOG = LogFactory.getLog(DocumentNotFoundException.class);

    public DocumentNotFoundException(String message, Throwable cause) {
        super(message, cause);
        LOG.error(message);
    }

    public DocumentNotFoundException(String message) {
        super(message);
        LOG.error(message);
    }

    public DocumentNotFoundException(Throwable cause) {
        super(cause);
    }

}
