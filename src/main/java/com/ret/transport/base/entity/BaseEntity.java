package com.ret.transport.base.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.transaction.annotation.Transactional;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;
import com.ret.transport.base.util.TimeZoneConversionUtil;
import com.ret.transport.base.util.Validation;

/**
 * This class provides default metadata column mappings for entity classes All entity classes should
 * extend this class
 *
 * @author Vishnu on 01-Oct-2017
 */
@MappedSuperclass
@Transactional(value = "jpaTransactionManager")
public abstract class BaseEntity implements Serializable {

    private static final long serialVersionUID = 9046320378495599743L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "modified_by")
    private Long modifiedBy;

    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(LocalDateTime modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public void setCreateInfo(Long createdBy) {
        if (Validation.isNull(createdBy)) {
            ExceptionUtil.throwException(EExceptionType.UNAUTHORIZED, "Unauthorized User");
        }
        setStatus(true);
        setCreatedBy(createdBy);
        setModifiedBy(createdBy);
        setCreatedAt(TimeZoneConversionUtil.getLocalDateTime());
        setModifiedAt(TimeZoneConversionUtil.getLocalDateTime());
    }

    public void setUpdateInfo(Long modifiedBy) {
        if (Validation.isNull(modifiedBy)) {
            ExceptionUtil.throwException(EExceptionType.UNAUTHORIZED, "Unauthorized User");
        }
        setModifiedBy(modifiedBy);
        setModifiedAt(TimeZoneConversionUtil.getLocalDateTime());
    }
}
