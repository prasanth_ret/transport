package com.ret.transport.base.exception.handler;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ret.transport.base.enums.EResponseStatusCode;
import com.ret.transport.base.exception.service.BadRequestException;
import com.ret.transport.base.io.BaseResponse;

/**
 * @author Vishnu on 08-Feb-2018
 */
@ControllerAdvice
public class BadRequestExceptionHandler {

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public BaseResponse badRequest(BadRequestException exception) {
        BaseResponse response = new BaseResponse(EResponseStatusCode.FAILURE);
        response.setErrorCode(HttpServletResponse.SC_BAD_REQUEST);
        response.setErrorMessage(exception.getMessage());
        return response;
    }
}
