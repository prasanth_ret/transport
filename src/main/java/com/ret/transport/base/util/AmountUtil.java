package com.ret.transport.base.util;

import java.text.DecimalFormat;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class AmountUtil {

    public static String DECIMAL_FORMAT_0_00 = "#0.00";
    public static String AMOUNT_0_00 = "0.00";

    public static String getDecimalValueofAmtStr(Double amount) {
        if (amount != null) {
            return new DecimalFormat(DECIMAL_FORMAT_0_00).format(amount);
        }
        return "";
    }

    public static Double getDecimalValueofAmt(Double amount) {
        if (amount != null) {
            String amtStr = new DecimalFormat(DECIMAL_FORMAT_0_00).format(amount);
            return Double.valueOf(amtStr);
        }
        return amount;
    }

    public static Double stringToDouble(String amount) {
        if (Validation.isNotEmpty(amount)) {
            return Double.valueOf(amount);
        }
        return null;
    }
}
