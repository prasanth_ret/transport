package com.ret.transport.base.enums;

import java.util.HashMap;
import java.util.Map;

public enum ETimeDiffers {

    YEAR("Year"), 
    MONTH("month"),
    WEEK("Week"),
    DAY("Day"),
    HOUR("Hour"),
    MINUTE("Minute"),
    SECOND("Second"),
    MILLI_SECOUND("Milli Second");


    private String value;

    ETimeDiffers(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    
    private static final Map<String, ETimeDiffers> timeDifferMap = new HashMap<String, ETimeDiffers>();
    static {
        for (ETimeDiffers timeDiffer : values())
            timeDifferMap.put(timeDiffer.getValue(), timeDiffer);
    }

    public static ETimeDiffers getEnum(String role) {
        return timeDifferMap.get(role);
    }
}
