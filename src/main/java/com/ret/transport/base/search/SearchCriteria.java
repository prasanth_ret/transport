package com.ret.transport.base.search;

import com.ret.transport.base.search.enums.ESearchOperator;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class SearchCriteria {
    private String key;
    private ESearchOperator operator;
    private Object value;

    public SearchCriteria(String key, ESearchOperator operator, Object value) {
        this.key = key;
        this.operator = operator;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public ESearchOperator getOperator() {
        return operator;
    }

    public Object getValue() {
        return value;
    }
}
