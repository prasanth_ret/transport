package com.ret.transport.user.service;

import org.springframework.security.core.Authentication;

import com.ret.transport.user.entity.User;

public interface UserService {

    User checkAuthUser(Authentication authentication);

    User getUserByEmail(String email);

    User getUserByPhoneNumber(String phoneNumber);

    User findUserByEmail(String email);

    User findUserByPhoneNumber(String phoneNumber);

}
