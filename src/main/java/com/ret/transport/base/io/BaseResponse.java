package com.ret.transport.base.io;

import java.io.Serializable;

import com.ret.transport.base.enums.EResponseStatusCode;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class BaseResponse implements Serializable {
	private static final long serialVersionUID = -2986104866988135405L;

	private EResponseStatusCode status;
	private int errorCode;
	private String errorMessage;
	

	public BaseResponse() {
		this(EResponseStatusCode.SUCCESS);
	}

	public BaseResponse(EResponseStatusCode status) {
		this.status = status;
	}

	public EResponseStatusCode getStatus() {
		return status;
	}

	public void setStatus(EResponseStatusCode status) {
		this.status = status;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setFailure(int errorCode, String errorMessage) {
		setStatus(EResponseStatusCode.FAILURE);
		setErrorCode(errorCode);
		setErrorMessage(errorMessage);
	}

	public void setFailure(String errorMessage) {
		setStatus(EResponseStatusCode.FAILURE);
		setErrorMessage(errorMessage);
	}
}
