package com.ret.transport.hireSlip.dto;

import com.ret.transport.base.util.ConstDateFormat;
import com.ret.transport.base.util.DateConversionUtil;
import com.ret.transport.hireSlip.entity.LRForm;

public class LRFormConversionUtil {

    public static LRFormRequestDTO convertEntityToDTO(LRForm lrForm) {
        LRFormRequestDTO dto = new LRFormRequestDTO();
        dto.setId(lrForm.getId());
        dto.setDate(DateConversionUtil.convertDate(lrForm.getDate(), ConstDateFormat.jsonDateFormat));

        dto.setLrNumber(lrForm.getLrNumber());
        dto.setChargedKgs(lrForm.getChargedKgs());
        dto.setFreightToPay(lrForm.getFreightToPay());
        dto.setUnloadingCharges(lrForm.getUnloadingCharges());
        dto.setRatePerBox(lrForm.getRatePerBox());
        dto.setActualKgs(lrForm.getActualKgs());
        dto.setTotal(lrForm.getTotal());
        return dto;
    }

}
