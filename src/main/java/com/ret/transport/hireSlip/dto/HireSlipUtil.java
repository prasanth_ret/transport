package com.ret.transport.hireSlip.dto;

import com.ret.transport.base.util.BaseCommonMessageUtil;

public interface HireSlipUtil extends BaseCommonMessageUtil {

    String HIRE_SLIP_NOT_FOUND = "Hire Slip Not Found";

    String LR_NUMBER_NOT_FOUND = "LR Number Not Found";

    String PAYMENT_DETAILS_NOT_FOUND = "Payment Detials Not Found";

    String LR_FORM_NOT_FOUND = "LR Form Not Found";

    String FROM_DATE_IS_REQUIRED = "From Date Is Required";

    String TO_DATE_IS_REQUIRED = "To Date Is Required";

    String HIRE_SLIP_DATE_NOT_FOUND = "Hire Slip Date Not Found";

    String TOTAL_WEIGHT_NOT_FOUND = "Total Weight Not Found";

    String RATE_PER_TON_NOT_FOUND = "Rate Per Ton Not Found";

    String LORRY_HIRE_AMT_NOT_FOUND = "Lorry Hire Amt Not Found";

    String OTHER_CHARGES_NOT_FOUND = "Other Charges Not Found";

    String TOTAL_HIRE_AMT_NOT_FOUND = "Total Hire Amt Not Found";

    String BALANCE_AMT_NOT_FOUND = "Balance Amt Not Found";

    String ESTIMATED_DELIVERY_DATE_NOT_FOUND = "Estimated Delivery Date Not Found";

    String STATISTICAL_CHARGES_NOT_FOUND = "Statistical Charges Not Found";

    String CGST_AMT_NOT_FOUND = "CGST Amt Not Found";

    String SGST_AMT_NOT_FOUND = "SGST Amt Not Found";

    String IGST_NOT_FOUND = "IGST Amt Not Found";

    String TOTAL_AMT_NOT_FOUND = "Total Amt Not Found";

    String VALUE_NOT_FOUND = "Value Not Found";

    String DATE_NOT_FOUND = "LR Date Not Found";

    String FROM_CITY_SHOULD_NOT_BE_NULL = "From City Should Not Be Null";

    String TO_CITY_SHOULD_NOT_BE_NULL = "To City Should Not Be Null";

    String AGENT_FIELD_SHOULD_NOT_BE_NULL = "Agent Field Should Not Be Null";

    String LORRY_DRIVER_SHOULD_NOT_BE_NULL = "Lorry Driver Should Not Be Null";

    String NO_OF_PACKAGES_NOT_FOUND = "Numbre Of Packages Not Found";

    String DESCRIPTION_OF_GOODS_NOT_FOUND = "Description Of Goods Not Found";

    String ACTUAL_KG_NOT_FOUND = "Actula Kg Not Found";

    String CHARGED_KG_NOT_FOUND = "Charged Kg Not Found";

    String FREIGHT_TO_PAY_NOT_FOUND = "Freight To Pay Not Found";

    String PAYMENT_MODE_NOT_FOUND = "Payment Mode Not Found";

    String AMT_PAID_NOT_FOUND = "Amount Paid Not Found";

    String PAYMENT_REFERENCE_NUMBER_NOT_FOUND = "Payment Reference Nomber Not Found";

    String PAYMENT_DATE_NOT_FOUND = "Payment Date Not Found";

    String INVALID_LR_TO_UPDATE = "In-Valid LR to Update";

    String INVALID_BILL_NUMBER = "Invalid Bill Number";

    String INVALID_BILL_DATE = "Invalid Bill Date";

    String PARTY_INVOICE_NOT_FOUND = "Party Invoice Not Found";

    String UNLOADING_CHARGES_NOT_FOUND = "Unloading Charges Not Found";

    String BILL_NUMBER_ALREADY_EXIST = "Bill Number Already Exist";

}
