package com.ret.transport.hireSlip.dto;

import com.ret.transport.base.util.BaseCommonMessageUtil;

public interface CustomerInfoMessageUtil extends BaseCommonMessageUtil {

    String LR_NUMBER_LIST_EMPTY = "LR Number List Empty";

    String LIST_EMPTY = "List Empty";

    String ORGANAISATION_NAME_SHOULD_NOT_BE_NULL = "Organaisation Name Should Not Be Null";

    String CUSTOMER_BRANCH_DETAILS_SHOULD_NOT_BE_NULL = "Customer Branch Details Should Not Be Null";

    String CUSTOMER_NOT_FOUND = "Customer Not Found";

    String INVALID_AGENT_ID = "Invalid Agent ID";

    String AGENT_NAME_SHOULD_BE_MANDATORY = "Agent Name should be mandatory";

    String CUSTOMER_BRANCH_NOT_FOUND = "Customer Branch Not Found";

    String CUSTOMER_INFO_DETAILS_NOT_FOUND = "Customer Branch Details Not Found For Given Id";

}
