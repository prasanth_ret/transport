package com.ret.transport.hireSlip.dto;

import com.ret.transport.base.search.SearchParam;

public class LRFormResponseDTO extends SearchParam {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Long loginId;
    private Double cgst;
    private Double sgst;
    private Double igst;
    private Double total;
    private Double valueRs;
    private Double unloadingCharges;
    private String eWayBillNumber;
    private String fromDate;
    private String toDate;
    private Long lrFormId;
    private String lrNumber;
    private String lorryNumber;
    private String consignorName;
    private String consigneeName;

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getLorryNumber() {
        return lorryNumber;
    }

    public void setLorryNumber(String lorryNumber) {
        this.lorryNumber = lorryNumber;
    }

    public Long getLrFormId() {
        return lrFormId;
    }

    public void setLrFormId(Long lrFormId) {
        this.lrFormId = lrFormId;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public Double getCgst() {
        return cgst;
    }

    public void setCgst(Double cgst) {
        this.cgst = cgst;
    }

    public Double getSgst() {
        return sgst;
    }

    public void setSgst(Double sgst) {
        this.sgst = sgst;
    }

    public Double getIgst() {
        return igst;
    }

    public void setIgst(Double igst) {
        this.igst = igst;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getValueRs() {
        return valueRs;
    }

    public void setValueRs(Double valueRs) {
        this.valueRs = valueRs;
    }

    public Double getUnloadingCharges() {
        return unloadingCharges;
    }

    public void setUnloadingCharges(Double unloadingCharges) {
        this.unloadingCharges = unloadingCharges;
    }

    public String geteWayBillNumber() {
        return eWayBillNumber;
    }

    public void seteWayBillNumber(String eWayBillNumber) {
        this.eWayBillNumber = eWayBillNumber;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getLrNumber() {
        return lrNumber;
    }

    public void setLrNumber(String lrNumber) {
        this.lrNumber = lrNumber;
    }

}
