package com.ret.transport.base.exception.jwt.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.AuthenticationException;

/**
 * @author Vishnu on 30-Jan-2018
 */
public class JWTTokenMissingException extends AuthenticationException {

    private static final long serialVersionUID = 8662397225573404928L;

    private static final Log LOG = LogFactory.getLog(JWTTokenMalformedException.class);

    public JWTTokenMissingException(String message, Throwable cause) {
        super(message, cause);
        LOG.error(message);
    }

    public JWTTokenMissingException(String message) {
        super(message);
        LOG.error(message);
    }

}
