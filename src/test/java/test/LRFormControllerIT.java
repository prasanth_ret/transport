package test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.ret.transport.hireSlip.dto.LRFormRequestDTO;
import com.ret.transport.login.dto.LoginRequestDTO;

import base.BaseIT;
import base.BaseUtil;

public class LRFormControllerIT extends BaseIT {
    private final String LOGIN_URL = "/api/user/login";
    private final String LR_LIST_URL = "/rest/api/lrs";

    private String getTokenBasedOnLoginCredentials(String userName, String password) throws Exception {
        LoginRequestDTO loginDTO = new LoginRequestDTO();
        loginDTO.setUserName(userName);
        loginDTO.setPassword(password);
        MvcResult mvcResult = mockMvc
                .perform(post(LOGIN_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(BaseUtil.convertObjToJson(loginDTO)))
                .andExpect(status().isOk()).andExpect(jsonPath("$.status").value("SUCCESS")).andReturn();
        JSONObject jsonObject = new JSONObject(mvcResult.getResponse().getContentAsString());
        JSONObject jsonResult = jsonObject.getJSONObject("result");
        return (String) jsonResult.get("token");
    }

    @Test
    public void getLrList() throws Exception {
        String token = getTokenBasedOnLoginCredentials("9874563210", "123456");
        System.out.println(token);
        LRFormRequestDTO requestDTO = new LRFormRequestDTO();
        requestDTO.setDataLength(10);
        requestDTO.setPageIndex(0);
        String tokens = "Bearer " + token;
        mockMvc.perform(post(LR_LIST_URL).header("Authorization", tokens).contentType(MediaType.APPLICATION_JSON)
                .content(BaseUtil.convertObjToJson(requestDTO))).andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"));
    }

}
