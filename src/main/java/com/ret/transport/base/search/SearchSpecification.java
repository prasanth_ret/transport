package com.ret.transport.base.search;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.ret.transport.base.search.enums.ESearchOperator;
import com.ret.transport.base.util.Validation;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class SearchSpecification<T> implements Specification<T> {

    private final SearchCriteria searchCriteria;

    public SearchSpecification(SearchCriteria searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Predicate predicate = null;
        switch (searchCriteria.getOperator()) {
        case GREATER_THAN:
            predicate = greaterThan(root, criteriaBuilder);
            break;
        case GREATER_THAN_OR_EQUAL:
            predicate = greaterThanOrEqual(root, criteriaBuilder);
            break;
        case LESS_THAN:
            predicate = lessThan(root, criteriaBuilder);
            break;
        case LESS_THAN_OR_EQUAL:
            predicate = lessThanOrEqual(root, criteriaBuilder);
            break;
        case NOT_EQUAL:
            predicate = criteriaBuilder.notEqual(getFieldPath(searchCriteria.getKey(), root),
                    searchCriteria.getValue().toString());
            break;
        case EQUAL:
            predicate = criteriaBuilder.equal(getFieldPath(searchCriteria.getKey(), root), searchCriteria.getValue());
            break;
        case LIKE:
            Path<Object> likePath = getFieldPath(searchCriteria.getKey(), root);
            if (likePath.getJavaType() == String.class) {
                predicate = criteriaBuilder.like(getPath(String.class, root, searchCriteria.getKey()),
                        "%" + searchCriteria.getValue() + "%");
            }
            break;
        case LIKE_START_WITH:
            Path<Object> likeStartPath = getFieldPath(searchCriteria.getKey(), root);
            if (likeStartPath.getJavaType() == String.class) {
                predicate = criteriaBuilder.like(getPath(String.class, root, searchCriteria.getKey()),
                        searchCriteria.getValue() + "%");
            }
            break;
        case LIKE_END_WITH:
            Path<Object> likeEndPath = getFieldPath(searchCriteria.getKey(), root);
            if (likeEndPath.getJavaType() == String.class) {
                predicate = criteriaBuilder.like(getPath(String.class, root, searchCriteria.getKey()),
                        "%" + searchCriteria.getValue());
            }
            break;
        case NOT_LIKE_START_WITH:
            Path<Object> notLikeStartPath = getFieldPath(searchCriteria.getKey(), root);
            if (notLikeStartPath.getJavaType() == String.class) {
                predicate = criteriaBuilder.notLike(getPath(String.class, root, searchCriteria.getKey()),
                        searchCriteria.getValue() + "%");
            }
            break;
        case NOT_LIKE_END_WITH:
            Path<Object> notLikeEndPath = getFieldPath(searchCriteria.getKey(), root);
            if (notLikeEndPath.getJavaType() == String.class) {
                predicate = criteriaBuilder.notLike(getPath(String.class, root, searchCriteria.getKey()),
                        "%" + searchCriteria.getValue());
            }
            break;

        case NOT_LIKE:
            Path<Object> notLikePath = getFieldPath(searchCriteria.getKey(), root);
            if (notLikePath.getJavaType() == String.class) {
                predicate = criteriaBuilder.notLike(getPath(String.class, root, searchCriteria.getKey()),
                        "%" + searchCriteria.getValue() + "%");
            }
            break;
        case IN:
            if (searchCriteria.getValue() instanceof List<?>) {
                predicate = getFieldPath(searchCriteria.getKey(), root)
                        .in(((List<?>) searchCriteria.getValue()).toArray());
            }
            break;
        case NOT_IN:
            predicate = inQuery(root, criteriaBuilder, searchCriteria.getOperator());
            break;
        case BETWEEN:
            predicate = between(root, criteriaBuilder);
            break;
        case NULL:
            predicate = criteriaBuilder.isNull(getPath(String.class, root, searchCriteria.getKey()));
            break;
        case NOT_NULL:
            predicate = criteriaBuilder.isNotNull(getPath(String.class, root, searchCriteria.getKey()));
            break;
        default:
            break;
        }
        return predicate;
    }

    private Path<Object> getFieldPath(String key, Root<T> root) {
        Path<Object> fieldPath = null;
        if (key.contains(".")) {
            String[] fields = key.split("\\.");
            for (String field : fields) {
                if (fieldPath == null) {
                    fieldPath = root.get(field);
                } else {
                    fieldPath = fieldPath.get(field);
                }
            }
        } else {
            fieldPath = root.get(key);
        }
        return fieldPath;
    }

    @SuppressWarnings({ "hiding", "unchecked" })
    private <T, Object> Path<Object> getPath(Class<Object> clazz, Root<T> root, String key) {
        String[] fields = key.split("\\.");
        Path<?> path = root;
        for (String field : fields) {
            path = (Path<Object>) path.get(field);
        }
        return (Path<Object>) path;
    }

    private Predicate greaterThan(Root<T> root, CriteriaBuilder criteriaBuilder) {
        Predicate predicate = null;
        if (searchCriteria.getValue() instanceof LocalDateTime) {
            predicate = criteriaBuilder.greaterThan(getPath(LocalDateTime.class, root, searchCriteria.getKey()),
                    (LocalDateTime) searchCriteria.getValue());
        } else if (searchCriteria.getValue() instanceof Date) {
            predicate = criteriaBuilder.greaterThan(getPath(Date.class, root, searchCriteria.getKey()),
                    (Date) searchCriteria.getValue());
        } else {
            predicate = criteriaBuilder.greaterThan(getPath(String.class, root, searchCriteria.getKey()),
                    searchCriteria.getValue().toString());
        }
        return predicate;
    }

    private Predicate lessThan(Root<T> root, CriteriaBuilder criteriaBuilder) {
        Predicate predicate = null;
        if (searchCriteria.getValue() instanceof LocalDateTime) {
            predicate = criteriaBuilder.lessThan(getPath(LocalDateTime.class, root, searchCriteria.getKey()),
                    (LocalDateTime) searchCriteria.getValue());
        } else if (searchCriteria.getValue() instanceof Date) {
            predicate = criteriaBuilder.lessThan(getPath(Date.class, root, searchCriteria.getKey()),
                    (Date) searchCriteria.getValue());
        } else {
            predicate = criteriaBuilder.lessThan(getPath(String.class, root, searchCriteria.getKey()),
                    searchCriteria.getValue().toString());
        }
        return predicate;
    }

    private Predicate greaterThanOrEqual(Root<T> root, CriteriaBuilder criteriaBuilder) {
        Predicate predicate = null;
        if (searchCriteria.getValue() instanceof LocalDateTime) {
            predicate = criteriaBuilder.greaterThanOrEqualTo(
                    getPath(LocalDateTime.class, root, searchCriteria.getKey()),
                    (LocalDateTime) searchCriteria.getValue());
        } else if (searchCriteria.getValue() instanceof Date) {
            predicate = criteriaBuilder.greaterThanOrEqualTo(getPath(Date.class, root, searchCriteria.getKey()),
                    (Date) searchCriteria.getValue());
        } else {
            predicate = criteriaBuilder.greaterThanOrEqualTo(getPath(String.class, root, searchCriteria.getKey()),
                    searchCriteria.getValue().toString());
        }
        return predicate;
    }

    private Predicate lessThanOrEqual(Root<T> root, CriteriaBuilder criteriaBuilder) {
        Predicate predicate = null;
        if (searchCriteria.getValue() instanceof LocalDateTime) {
            predicate = criteriaBuilder.lessThanOrEqualTo(getPath(LocalDateTime.class, root, searchCriteria.getKey()),
                    (LocalDateTime) searchCriteria.getValue());
        } else if (searchCriteria.getValue() instanceof Date) {
            predicate = criteriaBuilder.lessThanOrEqualTo(getPath(Date.class, root, searchCriteria.getKey()),
                    (Date) searchCriteria.getValue());
        } else {
            predicate = criteriaBuilder.lessThanOrEqualTo(getPath(String.class, root, searchCriteria.getKey()),
                    searchCriteria.getValue().toString());
        }
        return predicate;
    }

    private Predicate between(Root<T> root, CriteriaBuilder criteriaBuilder) {
        Predicate predicate = null;
        if (searchCriteria.getValue() instanceof List<?>) {
            List<?> range = (ArrayList<?>) searchCriteria.getValue();
            if (range.size() == 2) {
                if (range.get(0) instanceof Double && range.get(1) instanceof Double) {
                    Double from = (Double) range.get(0);
                    Double to = (Double) range.get(1);
                    predicate = criteriaBuilder.between(getPath(Double.class, root, searchCriteria.getKey()), from, to);
                } else if (range.get(0) instanceof LocalDateTime && range.get(1) instanceof LocalDateTime) {
                    LocalDateTime from = (LocalDateTime) range.get(0);
                    LocalDateTime to = (LocalDateTime) range.get(1);
                    predicate = criteriaBuilder.between(getPath(LocalDateTime.class, root, searchCriteria.getKey()),
                            from, to);
                } else if (range.get(0) instanceof Date && range.get(1) instanceof Date) {
                    Date from = (Date) range.get(0);
                    Date to = (Date) range.get(1);
                    predicate = criteriaBuilder.between(getPath(Date.class, root, searchCriteria.getKey()), from, to);
                } else {
                    predicate = criteriaBuilder.between(getPath(String.class, root, searchCriteria.getKey()),
                            range.get(0).toString(), range.get(0).toString());
                }
            }
        }
        return predicate;
    }

    private Predicate likeQuery(Root<T> root, CriteriaBuilder criteriaBuilder, ESearchOperator eOperator) {
        Path<Object> likePath = getFieldPath(searchCriteria.getKey(), root);
        String value = null;
        if (eOperator.equals(ESearchOperator.LIKE) || eOperator.equals(ESearchOperator.NOT_LIKE)) {
            value = String.format("%%s%", searchCriteria.getValue());
        } else if (eOperator.equals(ESearchOperator.LIKE_START_WITH)) {
            value = String.format("%s%", searchCriteria.getValue());
        } else if (eOperator.equals(ESearchOperator.LIKE_END_WITH)) {
            value = String.format("%%s", searchCriteria.getValue());
        }
        if (Validation.isNotNull(value) && likePath.getJavaType() == String.class) {
            if (eOperator.equals(ESearchOperator.NOT_LIKE)) {
                return criteriaBuilder.notLike(getPath(String.class, root, searchCriteria.getKey()), value);
            } else {
                return criteriaBuilder.like(getPath(String.class, root, searchCriteria.getKey()), value);
            }
        }
        return null;
    }

    private Predicate inQuery(Root<T> root, CriteriaBuilder criteriaBuilder, ESearchOperator eOperator) {
        if (searchCriteria.getValue() instanceof List<?>) {
            if (eOperator.equals(ESearchOperator.IN)) {
                return getFieldPath(searchCriteria.getKey(), root).in(((List<?>) searchCriteria.getValue()).toArray());
            } else if (eOperator.equals(ESearchOperator.NOT_IN)) {
                return criteriaBuilder.not(getFieldPath(searchCriteria.getKey(), root)
                        .in(((List<?>) searchCriteria.getValue()).toArray()));
            }
        }
        return null;
    }
}
