package com.ret.transport.security.handler;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;

/**
 * @author Vishnu on 30-Jan-2018
 */
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
            AuthenticationException e) throws IOException, ServletException {
        // httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        ExceptionUtil.throwException(EExceptionType.UNAUTHORIZED, "Unauthorized User");
    }
}
