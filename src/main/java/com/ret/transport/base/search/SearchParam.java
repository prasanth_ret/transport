package com.ret.transport.base.search;

import java.io.Serializable;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class SearchParam implements Serializable {

    private static final long serialVersionUID = 1L;
    // Request Variables
    private int pageIndex = 0;
    private int dataLength = 0;
    private String sortingColumn; // Separated by commas for multiple column sorting
    private String sortDirection; // Separated by commas for multiple column sorting Direction

    // Response Variables
    private int totalPages;
    private int totalData;

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getDataLength() {
        return dataLength;
    }

    public void setDataLength(int dataLength) {
        this.dataLength = dataLength;
    }

    public String getSortingColumn() {
        return sortingColumn;
    }

    public void setSortingColumn(String sortingColumn) {
        this.sortingColumn = sortingColumn;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalData() {
        return totalData;
    }

    public void setTotalData(int totalData) {
        this.totalData = totalData;
    }
}
