package com.ret.transport.base.exception.jwt.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.AuthenticationException;

/**
 * @author Vishnu on 30-Jan-2018
 */
public class JWTTokenMalformedException extends AuthenticationException {

    private static final long serialVersionUID = 5317799173009829157L;

    private static final Log LOG = LogFactory.getLog(JWTTokenMalformedException.class);

    public JWTTokenMalformedException(String message, Throwable cause) {
        super(message, cause);
        LOG.error(message);
    }

    public JWTTokenMalformedException(String message) {
        super(message);
        LOG.error(message);
    }

}
