package com.ret.transport.base.enums;

/**
 * @author Vishnu on 01-Oct-2017
 */
public enum EDateTimeType {

    EOD, // End Of Day
    SOD, // Start Of Day
    SOY, // Start Of Year
    EOY, // End Of Year
    SOM, // Start Of Month
    EOM; // End Of Month

}
