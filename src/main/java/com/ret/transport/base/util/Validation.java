package com.ret.transport.base.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.ObjectError;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class Validation {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final String PHONE_NUM_PATTERN = "^[0-9]{10}$";

    public static boolean isNotNull(Object obj) {
        return obj != null;
    }

    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }

    public static boolean isNotEmpty(String str) {
        return str != null && !str.trim().isEmpty();
    }

    public static boolean isEmpty(Collection coll) {
        return coll == null || coll.isEmpty();
    }

    public static boolean isNotEmpty(Collection coll) {
        return coll != null && !coll.isEmpty();
    }

    public static boolean isEmpty(Map map) {
        return map == null || map.isEmpty();
    }

    public static boolean isNotEmpty(Map map) {
        return map != null && !map.isEmpty();
    }

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public static boolean isNotZero(Object obj) {
        if (isNotNull(obj)) {
            if (obj instanceof String) {
                String str = (String) obj;
                return isNotEmpty(str) && (str != "0.0" || str != "0");
            } else if (obj instanceof Double) {
                return (isNotNull((Double) obj) && (Double) obj != 0.0);
            } else if (obj instanceof Integer) {
                return (isNotNull((Integer) obj) && (Integer) obj != 0);
            } else if (obj instanceof Long) {
                return (isNotNull((Long) obj) && (Long) obj != 0l);
            } else if (obj instanceof Float) {
                return (isNotNull((Float) obj) && (Float) obj != 0.0);
            }
        }
        return false;
    }

    public static boolean isNullZero(Object obj) {
        if (isNotNull(obj)) {
            if (obj instanceof String) {
                String str = (String) obj;
                return isNotEmpty(str) && (str == "0.0" || str == "0");
            } else if (obj instanceof Double) {
                return (Double) obj == 0.0;
            } else if (obj instanceof Integer) {
                return (Integer) obj == 0;
            } else if (obj instanceof Long) {
                return (Long) obj == 0l;
            } else if (obj instanceof Float) {
                return (Float) obj == 0.0;
            }
        } else {
            return true;
        }
        return false;
    }

    public static boolean isZero(Object obj) {
        if (isNotNull(obj)) {
            if (obj instanceof String) {
                String str = (String) obj;
                return isNotEmpty(str) && (str.equals("0.0") || str.equals("0"));
            } else if (obj instanceof Double) {
                return (isNotNull((Double) obj) && (Double) obj == 0.0);
            } else if (obj instanceof Integer) {
                return (isNotNull((Integer) obj) && (Integer) obj == 0);
            } else if (obj instanceof Long) {
                return (isNotNull((Long) obj) && (Long) obj == 0l);
            } else if (obj instanceof Float) {
                return (isNotNull((Float) obj) && (Float) obj == 0.0);
            }
        }
        return false;
    }

    public static boolean checkIsNumeric(String value) {
        if (StringUtils.isNumeric(value)) {
            return true;
        } else {
            ExceptionUtil.throwException(EExceptionType.INVALID_DATA, value);
        }
        return false;
    }

    public static boolean emailValidator(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean phoneNumberValidator(String phoneNumber) {
        if (phoneNumber.matches(PHONE_NUM_PATTERN)) {
            return true;
        }
        return false;
    }

    public static String getBadRequestErrorMessage(List<ObjectError> objErrors) {
        List<String> errors = objErrors.stream().map(obj -> obj.getDefaultMessage()).collect(Collectors.toList());
        return CommonUtil.getStringWithSperator(errors, CommonUtil.COMMA);
    }
}
