package com.ret.transport.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;
import com.ret.transport.base.util.Validation;
import com.ret.transport.security.model.AuthenticatedUser;
import com.ret.transport.security.model.JwtAuthenticationToken;
import com.ret.transport.security.transfer.JWTUserDTO;
import com.ret.transport.security.util.JWTTokenValidator;

/**
 * @author Vishnu on 30-Jan-2018
 */
@Component
public class JWTAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private JWTTokenValidator jwtTokenValidator;

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails,
            UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;
        String token = jwtAuthenticationToken.getToken();
        JWTUserDTO parsedUser = jwtTokenValidator.parseToken(token);
        if (Validation.isNull(parsedUser)) {
            ExceptionUtil.throwException(EExceptionType.JWT_TOKEN_MALFORMED, "JWT token is not valid");
        }
        List<GrantedAuthority> authorityList = AuthorityUtils.commaSeparatedStringToAuthorityList(parsedUser.getRole());
        return new AuthenticatedUser(parsedUser.getUserName(), token, authorityList, parsedUser.getId());
    }

}
