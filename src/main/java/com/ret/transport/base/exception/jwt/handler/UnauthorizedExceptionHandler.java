package com.ret.transport.base.exception.jwt.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ret.transport.base.enums.EResponseStatusCode;
import com.ret.transport.base.exception.jwt.service.UnauthorizedException;
import com.ret.transport.base.io.BaseResponse;

/**
 * @author Vishnu on 30-Jan-2018
 */
@ControllerAdvice
public class UnauthorizedExceptionHandler {

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public BaseResponse dataNotFound(UnauthorizedException exception) {
        BaseResponse response = new BaseResponse(EResponseStatusCode.FAILURE);
        response.setErrorCode(100);
        response.setErrorMessage(exception.getMessage());
        return response;
    }
}
