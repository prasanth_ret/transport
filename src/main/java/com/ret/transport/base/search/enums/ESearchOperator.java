package com.ret.transport.base.search.enums;

/**
 * @author Vishnu on 01-Oct-2017
 */
public enum ESearchOperator {
    EQUAL("="), 
    NOT_EQUAL("!="), 
    NEGATION("!"), 
    GREATER_THAN(">"), 
    GREATER_THAN_OR_EQUAL(">="), 
    LESS_THAN("<"), 
    LESS_THAN_OR_EQUAL("<="), 
    LIKE("Like"), 
    NOT_LIKE("Not_Like"), 
    LIKE_START_WITH("Like_Start_With"), 
    LIKE_END_WITH("Like_End_With"), 
    NOT_LIKE_END_WITH("Not_Like_End_Width"), 
    NOT_LIKE_START_WITH("Not_Like_End_Width"), 
    IN(":"), 
    NOT_IN("!:"), 
    BETWEEN("Between"), 
    NULL("Null"), 
    NOT_NULL("Not_Null");

    private String value;

    ESearchOperator(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
