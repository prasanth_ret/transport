package com.ret.transport.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ret.transport.user.entity.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

    Address findByIdAndStatusTrue(Long id);

}
