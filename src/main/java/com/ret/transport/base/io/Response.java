package com.ret.transport.base.io;

import com.ret.transport.base.enums.EResponseStatusCode;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class Response<T> extends BaseResponse {
    private static final long serialVersionUID = -2274179846560709059L;

    private T result;

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public void setSuccess(T result) {
        setStatus(EResponseStatusCode.SUCCESS);
        setResult(result);
    }

    public void setSuccess() {
        setStatus(EResponseStatusCode.SUCCESS);
    }
}
