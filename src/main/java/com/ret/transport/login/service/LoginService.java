package com.ret.transport.login.service;

import com.ret.transport.login.dto.LoginRequestDTO;
import com.ret.transport.user.entity.User;

public interface LoginService {

    User loginAuthentication(LoginRequestDTO loginModel);

}
