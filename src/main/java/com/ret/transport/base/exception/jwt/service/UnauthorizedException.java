package com.ret.transport.base.exception.jwt.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Vishnu on 30-Jan-2018
 */
public class UnauthorizedException extends RuntimeException {
    private static final long serialVersionUID = 3481889740944533624L;

    private static final Log LOG = LogFactory.getLog(UnauthorizedException.class);

    public UnauthorizedException(String message, Throwable cause) {
        super(message, cause);
        LOG.error(message);
    }

    public UnauthorizedException(String message) {
        super(message);
        LOG.error(message);
    }

    public UnauthorizedException(Throwable cause) {
        super(cause);

    }

}
