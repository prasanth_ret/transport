package com.ret.transport.login.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ret.transport.base.enums.EExceptionType;
import com.ret.transport.base.exception.ExceptionUtil;
import com.ret.transport.base.util.Validation;
import com.ret.transport.login.dto.LoginRequestDTO;
import com.ret.transport.login.service.LoginService;
import com.ret.transport.user.entity.User;
import com.ret.transport.user.service.UserService;

@Service("loginService")
@Transactional(readOnly = false)
public class LoginServiceImpl implements LoginService {

    private final UserService userService;

    @Autowired
    public LoginServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public User loginAuthentication(LoginRequestDTO loginModel) {
        User user = getUser(loginModel);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        Boolean isPasswordValidate = encoder.matches(loginModel.getPassword(), user.getPassword());
        if (!isPasswordValidate) {
            ExceptionUtil.throwException(EExceptionType.UNAUTHORIZED, "User Name and Password does not match");
        }
        return user;
    }

    private User getUser(LoginRequestDTO loginModel) {
        Boolean isEmailValidator = Validation.emailValidator(loginModel.getUserName());
        Boolean isPhNumValidator = Validation.phoneNumberValidator(loginModel.getUserName());
        User user = null;
        if (isEmailValidator) {
            user = userService.findUserByEmail(loginModel.getUserName());
        } else if (isPhNumValidator) {
            user = userService.findUserByPhoneNumber(loginModel.getUserName());
        } else {
            ExceptionUtil.throwException(EExceptionType.INVALID_DATA, "Invalid User Name");
        }
        return user;
    }

}
