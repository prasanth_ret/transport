package com.ret.transport.security.transfer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;

import com.ret.transport.base.dto.BaseDTO;
import com.ret.transport.base.util.CommonUtil;

/**
 * @author Vishnu on 30-Jan-2018
 */
public class JWTUserDTO extends BaseDTO {

    private static final long serialVersionUID = 1L;

    private String userName;

    private String role;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public static JWTUserDTO buildFromAuthentication(Authentication authentication) {
        JWTUserDTO jwtUserDto = new JWTUserDTO();
        jwtUserDto.setRole(authentication.getAuthorities().toArray()[0].toString());
        jwtUserDto.setUserName(((User) authentication.getPrincipal()).getUsername());
        return jwtUserDto;
    }

    public static JWTUserDTO buildFromAuthentication(com.ret.transport.user.entity.User user) {
        JWTUserDTO jwtUserDto = new JWTUserDTO();
        List<String> roles = user.getUserRoleMaps().stream().map(obj -> obj.getRole().getName())
                .collect(Collectors.toList());
        jwtUserDto.setRole(CommonUtil.getStringWithSperator(roles, CommonUtil.COMMA));
        jwtUserDto.setUserName(user.getEmail());
        jwtUserDto.setId(user.getId());
        return jwtUserDto;
    }
}