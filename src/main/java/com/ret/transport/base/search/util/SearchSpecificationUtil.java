package com.ret.transport.base.search.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specifications;

import com.ret.transport.base.search.SearchCriteria;
import com.ret.transport.base.search.SearchParam;
import com.ret.transport.base.search.SearchSpecification;
import com.ret.transport.base.search.enums.ECondition;
import com.ret.transport.base.search.enums.ESearchOperator;
import com.ret.transport.base.util.Validation;

/**
 * @author Vishnu on 01-Oct-2017
 */
public class SearchSpecificationUtil {

    public static <T> Specifications<T> getSpecification(Specifications<T> spec, SearchCriteria searchCriteria,
            ECondition condition) {
        if (Validation.isNull(spec)) {
            return Specifications.where(new SearchSpecification<T>(searchCriteria));
        } else {
            if (Validation.isNotNull(searchCriteria.getValue())) {
                if (searchCriteria.getValue() instanceof List<?>) {
                    List<?> valueList = (ArrayList<?>) searchCriteria.getValue();
                    if (Validation.isEmpty(valueList)) {
                        return spec;
                    }
                } else if (searchCriteria.getValue() instanceof String) {
                    String valueStr = (String) searchCriteria.getValue();
                    if (Validation.isEmpty(valueStr)) {
                        return spec;
                    }
                }
            } else if (searchCriteria.getOperator().equals(ESearchOperator.NULL) == false
                    && searchCriteria.getOperator().equals(ESearchOperator.NOT_NULL) == false) {
                return spec;
            }
            if (Validation.isNotNull(condition)) {
                if (condition.equals(ECondition.AND)) {
                    spec = spec.and(new SearchSpecification<T>(searchCriteria));
                } else if (condition.equals(ECondition.OR)) {
                    spec = spec.or(new SearchSpecification<T>(searchCriteria));
                }
            }
        }
        return spec;
    }

    public static <T> SearchSpecification<T> getDefaultSpec() {
        return new SearchSpecification<T>(new SearchCriteria("status", ESearchOperator.EQUAL, true));
    }

    public static <T> SearchSpecification<T> getDefaultSpec(SearchCriteria searchCriteria) {
        return new SearchSpecification<T>(searchCriteria);
    }

    // Get the Pageable and Sort for filtering the data
    public static Pageable getPageableSort(SearchParam searchParam) {
        Sort sort = null;
        if (Validation.isNotEmpty(searchParam.getSortingColumn())
                && Validation.isNotEmpty(searchParam.getSortDirection())) {
            String[] columnArr = searchParam.getSortingColumn().split(",");
            String[] directionArr = searchParam.getSortDirection().split(",");
            for (int index = 0; index < columnArr.length; index++) {
                Sort sortObj = new Sort(
                        new Order(Direction.fromString(directionArr[index].trim()), columnArr[index].trim()));
                if (Validation.isNull(sort)) {
                    sort = sortObj;
                } else {
                    sort = sort.and(sortObj);
                }
            }
        }
        // If dataLength is null or 0, then the dataLength will be 2147483647
        if (Validation.isNull(searchParam.getDataLength()) || Validation.isZero(searchParam.getDataLength())) {
            searchParam.setDataLength(Integer.MAX_VALUE);
        }
        Pageable page = new PageRequest(searchParam.getPageIndex(), searchParam.getDataLength(), sort);
        return page;
    }

    // To Sort Data
    public static Sort getDataSort(SearchParam searchParam) {
        Sort sort = null;
        if (Validation.isNotEmpty(searchParam.getSortingColumn())
                && Validation.isNotEmpty(searchParam.getSortDirection())) {
            String[] columnArr = searchParam.getSortingColumn().split(",");
            String[] directionArr = searchParam.getSortDirection().split(",");
            for (int index = 0; index < columnArr.length; index++) {
                Sort sortObj = new Sort(
                        new Order(Direction.fromString(directionArr[index].trim()), columnArr[index].trim()));
                if (Validation.isNull(sort)) {
                    sort = sortObj;
                } else {
                    sort = sort.and(sortObj);
                }
            }
        }
        return sort;
    }

}
