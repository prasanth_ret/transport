package com.ret.transport.hireSlip.enums;

import java.util.HashMap;
import java.util.Map;

public enum ELRStatus {

    OPEN(1L, "Open"), BLOCKED(2L, "Blocked"), BOOKED(3L, "Booked");

    private Long id;
    private String value;

    ELRStatus(Long id, String value) {
        this.value = value;
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public Long getId() {
        return id;
    }

    private static final Map<String, ELRStatus> roleMap = new HashMap<String, ELRStatus>();
    static {
        for (ELRStatus role : values())
            roleMap.put(role.getValue(), role);
    }

    public static ELRStatus getEnum(String role) {
        return roleMap.get(role);
    }

    public static ELRStatus getEnum(int id) {
        switch (id) {
        case 1:
            return ELRStatus.OPEN;
        case 2:
            return ELRStatus.BLOCKED;
        case 3:
            return ELRStatus.BOOKED;
        default:
            return null;
        }
    }
}
